

# CMake scripts package

Provides the following features:
- Linter detection and setup (clang, cppcheck, gcc)
- Style enforcing (clang-format)
- API Documentation (doxygen)
- Coverage option (gcov)
- Packaging (RPM)
- Sub-projects and dependencies
- Badges (version, lint)

For an example integration see our [cpp-template](https://gitlab.cern.ch/mro/common/cpp/cpp-template)

## Adding to your project:

```bash
git submodule add https://gitlab.cern.ch/mro/common/cpp/cmake.git cmake

cp cmake/Init.cmake .

grep -q Init.cmake CMakeLists.txt || sed -i 's#\(cmake_minimum_required.*\)#\1\n\ninclude(./Init.cmake)#' CMakeLists.txt
```

## Managing dependencies

Create a `Dependencies.cmake` file in your project's root directory:
```CMake
# The following variables can be overriden (also from env):
# set(CCUT_SOURCE_DIR "deps/libccut") # non-default srcdir for submodule
# set(CCUT_GIT_SUBMODULE ON) # do not clone, update submodules
# set(CCUT_GIT_REPOSITORY "https://...")
# set(CCUT_GIT_TAG "master")
find_package(CCUT REQUIRED)

find_package(GRBL_SIM REQUIRED)
```

Then include it in your main `CMakeLists.txt` file:
```CMake
include(./Dependencies.cmake)
```

## Linting

To activate linting, include `Lint` module at the very end of your `CMakeLists.txt`.

Then run:
```bash
make lint

# This also activates styling (if clang-format is detected)
make style
```

## Coverage

To enable code-coverage, include `Coverage` module in your `CMakeLists.txt`.

To activate it, add `-DCOVERAGE=ON` when invoking CMake.

The following targets are also created:
```bash
# generate lcov report
make lcov

# generate lcov-result report
make lcov-result

# clean coverage information
make cov-clean
```

## Documentation

To enable API documentation, include `Doxygen` module in your `CMakeLists.txt`:

To generate the documentation (if doxygen is found):
```bash
make doc
```