# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

find_package(Git REQUIRED)

if(NOT CMAKE_SCRIPT_MODE_FILE AND NOT TARGET git_check)
    function(find_git_dir path)
        while(path AND NOT EXISTS "${path}/.git")
            get_filename_component(path "${path}" DIRECTORY)
        endwhile()
        if(EXISTS "${path}/.git")
            set(PROJECT_GIT_DIR "${path}/.git" PARENT_SCOPE)
        endif()
    endfunction()

    add_custom_target(git_check ALL DEPENDS ${CMAKE_BINARY_DIR}/.git_version)
    set(GIT_WORKDIR "${CMAKE_SOURCE_DIR}")
    find_git_dir("${PROJECT_SOURCE_DIR}")
    add_custom_command(
        OUTPUT ${CMAKE_BINARY_DIR}/.git_version
        COMMAND ${CMAKE_COMMAND} -DGIT_WORKDIR="${GIT_WORKDIR}" -P "${CMAKE_CURRENT_LIST_FILE}"
        DEPENDS ${PROJECT_GIT_DIR})

    include(GitBadge)
endif()

if (EXISTS "${CMAKE_SOURCE_DIR}/.git/shallow")
    # Use git fetch --unshallow to count commits
    message(STATUS "Count commits: shallow clone")
    set(GIT_COUNT "0")
else()
    execute_process(
        COMMAND ${GIT_EXECUTABLE} "log" "--pretty=format:"
        WORKING_DIRECTORY "${GIT_WORKDIR}"
        ERROR_FILE "${CMAKE_BINARY_DIR}/.git_error.log"
        OUTPUT_FILE ${CMAKE_BINARY_DIR}/.git_version)
    file(READ ${CMAKE_BINARY_DIR}/.git_version GIT_COUNT)
    string(LENGTH "${GIT_COUNT}" GIT_COUNT)
    file(WRITE ${CMAKE_BINARY_DIR}/.git_version ${GIT_COUNT})
    message(STATUS "Count commits: ${GIT_COUNT}")
endif()

execute_process(
    COMMAND ${GIT_EXECUTABLE} "rev-parse" "--short" "HEAD"
    WORKING_DIRECTORY "${GIT_WORKDIR}"
    OUTPUT_STRIP_TRAILING_WHITESPACE
    OUTPUT_VARIABLE GIT_SHORT)
execute_process(
    COMMAND ${GIT_EXECUTABLE} "status" "-s"
    WORKING_DIRECTORY "${GIT_WORKDIR}"
    OUTPUT_VARIABLE GIT_DIRTY)
if(GIT_DIRTY)
    message(STATUS "Git tree is dirty")
    set(GIT_DIRTY "+")
endif(GIT_DIRTY)
