
include(CheckLibraryExists)
include(Tools)

set_default(EXPLICIT_LIBC FALSE)
find_library(posix_C_LIBRARY c)

function(find_libc_component name fun)
    if(NOT DEFINED ${name}_IN_LIBC)
        # Check if libc bundles this module (see https://developers.redhat.com/articles/2021/12/17/why-glibc-234-removed-libpthread#)

        set(OLD_QUIET "${CMAKE_REQUIRED_QUIET}")
        set(CMAKE_REQUIRED_QUIET TRUE)

        check_library_exists(c ${fun} "${posix_C_LIBRARY}" ${name}_IN_LIBC)

        set(CMAKE_REQUIRED_QUIET "${OLD_QUIET}")

        if(${name}_IN_LIBC)
            message(STATUS "Found ${name} included in libc")
            set(posix_${name}_LIBRARY "${posix_C_LIBRARY}" CACHE PATH "Path to a library")
        endif()
    endif()
endfunction()

function(find_component name lib header fun)
    list(FIND "${posix_FIND_COMPONENTS}" ${name} WANT)
    if(WANT)
        find_libc_component(${name} ${fun})
        # Always prefer shared over static for those libraries
        find_library(posix_${name}_LIBRARY ${lib})
        find_path(posix_${name}_INCLUDE_DIR ${header})

        if(posix_${name}_LIBRARY AND posix_${name}_INCLUDE_DIR)
            set(posix_${name}_FOUND TRUE PARENT_SCOPE)

            if(NOT EXPLICIT_LIBRARY AND posix_${name}_LIBRARY STREQUAL posix_C_LIBRARY)
                add_library(posix::${name} INTERFACE IMPORTED)
                set_target_properties(posix::${name} PROPERTIES
                    INTERFACE_INCLUDE_DIRECTORIES "${posix_${name}_INCLUDE_DIR}")
            else()
                add_library(posix::${name} UNKNOWN IMPORTED)
                set_target_properties(posix::${name} PROPERTIES
                    INTERFACE_INCLUDE_DIRECTORIES "${posix_${name}_INCLUDE_DIR}"
                    IMPORTED_LINK_INTERFACE_LANGUAGES "C"
                    IMPORTED_LOCATION "${posix_${name}_LIBRARY}")
                list(APPEND _LIBS "${posix_${name}_LIBRARY}")
            endif()
            list(APPEND _INCS "${posix_${name}_INCLUDE_DIR}")
            set(_INCS "${_INCS}" PARENT_SCOPE)
            set(_LIBS "${_LIBS}" PARENT_SCOPE)
        else()
            set(posix_${name}_FOUND FALSE PARENT_SCOPE)
        endif()
    endif()
endfunction()

find_path(posix_INCLUDE_DIR stdint.h)
if(posix_INCLUDE_DIR AND NOT TARGET posix)
    set(_LIBS)
    set(_INCS)

    find_component(pthread pthread pthread.h pthread_create)
    find_component(dl dl dlfcn.h dlopen)
    find_component(rt rt time.h clock_gettime)

    if(stdatomic IN_LIST posix_FIND_COMPONENTS)
        set(TEST_FILE "${CMAKE_CURRENT_LIST_DIR}/Findposix-test.c")

        file(WRITE "${TEST_FILE}"
            "#include <stdatomic.h>\n"
        "int main() { atomic_int b = 1; return b; }")
        try_compile(posix_stdatomic_FOUND ${CMAKE_CURRENT_BINARY_DIR}
            SOURCES "${TEST_FILE}")
        file(REMOVE "${TEST_FILE}")
    endif()


    list(REMOVE_DUPLICATES _LIBS)
    list(REMOVE_DUPLICATES _INCS)
    if(NOT EXPLICIT_LIBC)
        list(REMOVE_ITEM _LIBS "${posix_C_LIBRARY}")
    endif()

    list(POP_FRONT _LIBS _LIB)

    if(_LIB)
        add_library(posix UNKNOWN IMPORTED)
        set_target_properties(posix PROPERTIES
            IMPORTED_LINK_INTERFACE_LANGUAGES "C"
            IMPORTED_LOCATION "${_LIB}"
            IMPORTED_LINK_INTERFACE_LIBRARIES "${_LIBS}"
            INTERFACE_INCLUDE_DIRECTORIES "${_INCS}")
    else()
        add_library(posix INTERFACE IMPORTED)
        set(posix PROPERTIES
            INTERFACE_INCLUDE_DIRECTORIES "${_INCS}")
    endif()
    unset(_LIBS)
    unset(_INCS)
    unset(_LIB)
endif()

find_package_handle_standard_args(posix
    REQUIRED_VARS posix_INCLUDE_DIR
    HANDLE_COMPONENTS)

