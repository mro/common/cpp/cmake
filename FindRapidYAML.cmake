
include(xProject)

find_package(Python3 REQUIRED)

set(RYML_INTERNALS_OUT "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_INCLUDEDIR}/ryml_internals.hpp")
xProject_Add(RapidYAML
    GIT_SUBMODULE
    BUILD_COMMAND ${Python3_EXECUTABLE}
        ${CMAKE_SOURCE_DIR}/deps/rapidyaml/tools/amalgamate.py "ryml_internals.hpp"
    INSTALL_COMMAND ${CMAKE_COMMAND} -E copy_if_different "ryml_internals.hpp" "${RYML_INTERNALS_OUT}"
    INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_INCLUDEDIR}/")
install(FILES "${RYML_INTERNALS_OUT}" DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}/ccut")

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(RapidYAML
    REQUIRED_VARS RapidYAML_INCLUDE_DIRS)
