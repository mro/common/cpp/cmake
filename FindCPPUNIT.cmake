# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(FindPackageHandleStandardArgs)
include(${CMAKE_CURRENT_LIST_DIR}/Tools.cmake)

if(CPPUNIT_FIND_QUIETLY)
    set(_CPPUNIT_FIND_QUIETLY "QUIET")
endif()

find_package(PkgConfig QUIET)
if (PkgConfig_FOUND)
    pkg_check_modules(CPPUNIT ${_CPPUNIT_FIND_QUIETLY} cppunit)
else()
    find_library(CPPUNIT_LINK_LIBRARIES cppunit ${_CPPUNIT_FIND_QUIETLY})
    find_path(CPPUNIT_INCLUDE_DIRS cppunit/TestFixture.h ${_CPPUNIT_FIND_QUIETLY})
    set(CPPUNIT_LIBRARIES "${CPPUNIT_LINK_LIBRARIES}")
endif()

unset(_CPPUNIT_FIND_QUIETLY)

if((CPPUNIT_LOCAL OR NOT CPPUNIT_FOUND) AND CPPUNIT_REPO_DIR AND NOT TARGET cppunit)
    if(NOT CPPUNIT_FIND_QUIETLY)
        message(STATUS "  Using local version of 'cppunit'")
    endif()
    set(CPPUNIT_LOCAL TRUE CACHE BOOL "local build" FORCE)
    set(CPPUNIT_FOUND TRUE CACHE BOOL "local build" FORCE)
    # public remote: git://anongit.freedesktop.org/git/libreoffice/cppunit/
    set_default(CPPUNIT_REMOTE_URL "https://gitlab.cern.ch/mro/third-party/cppunit.git")
    set_default(CPPUNIT_REMOTE_REF "master")

    if(NOT EXISTS ${CPPUNIT_REPO_DIR})
        find_package(Git REQUIRED)
        get_filename_component(DEPS_DIR "${CPPUNIT_REPO_DIR}" DIRECTORY)
        file(MAKE_DIRECTORY "${DEPS_DIR}")
        execute_process(COMMAND ${GIT_EXECUTABLE} clone -b "${CPPUNIT_REMOTE_REF}"
            "${CPPUNIT_REMOTE_URL}" "${CPPUNIT_REPO_DIR}"
            RESULT_VARIABLE RET)
        assert(RET EQUAL 0 MESSAGE "Failed to clone ${CPPUNIT_REMOTE_URL}" FATAL_ERROR)
    endif()
    file(GLOB_RECURSE CPPUNIT_SRC RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
        "${CPPUNIT_REPO_DIR}/src/cppunit/*.cpp"
        "${CPPUNIT_REPO_DIR}/src/cppunit/*.h")
    list(FILTER CPPUNIT_SRC EXCLUDE REGEX "DllMain.cpp")
    list(FILTER CPPUNIT_SRC EXCLUDE REGEX "UnixDynamicLibraryManager.cpp")

    set(CPPUNIT_INCLUDE_DIRS "${CPPUNIT_REPO_DIR}/include")
    set(CPPUNIT_LIBRARIES cppunit)
    set(CPPUNIT_LINK_LIBRARIES cppunit)
    add_library(cppunit STATIC ${CPPUNIT_SRC})

    set(CPPUNIT_TEST_FILE "${CMAKE_CURRENT_LIST_DIR}/FindCPPUNIT-test.cpp")

    file(WRITE "${CPPUNIT_TEST_FILE}"
        "#include <sstream>\n"
        "int main() { std::stringstream s; return 0; }")
    try_compile(CPPUNIT_HAVE_SSTREAM ${CMAKE_CURRENT_BINARY_DIR}
        SOURCES "${CPPUNIT_TEST_FILE}")

    file(WRITE "${CPPUNIT_TEST_FILE}"
        "#include <cmath>\n"
        "int main() { sqrtl(42.0); return 0; }")
    try_compile(CPPUNIT_HAVE_CMATH ${CMAKE_CURRENT_BINARY_DIR}
        SOURCES "${CPPUNIT_TEST_FILE}")

    file(WRITE "${CPPUNIT_TEST_FILE}"
        "#include <inttypes.h>\n"
        "int main() { return PRId64; }")
    try_compile(CPPUNIT_HAVE_INTTYPES_H ${CMAKE_CURRENT_BINARY_DIR}
        SOURCES "${CPPUNIT_TEST_FILE}")

    file(WRITE "${CPPUNIT_TEST_FILE}"
        "#include <memory.h>\n"
        "int main() { return 0; }")
    try_compile(CPPUNIT_HAVE_MEMORY_H ${CMAKE_CURRENT_BINARY_DIR}
        SOURCES "${CPPUNIT_TEST_FILE}")

    file(WRITE "${CPPUNIT_TEST_FILE}"
        "namespace test { const int i = 42; }\n"
        "int main() { return test::i; }")
    try_compile(CPPUNIT_HAVE_NAMESPACES ${CMAKE_CURRENT_BINARY_DIR}
        SOURCES "${CPPUNIT_TEST_FILE}")

    file(WRITE "${CPPUNIT_TEST_FILE}"
        "#include <strstream>\n"
        "int main() { std::strstream s; return 0; }")
    try_compile(CPPUNIT_HAVE_STRSTREAM ${CMAKE_CURRENT_BINARY_DIR}
        SOURCES "${CPPUNIT_TEST_FILE}")
    set(CPPUNIT_HAVE_CLASS_STRSTREAM "${CPPUNIT_HAVE_STRSTREAM}")

    file(REMOVE "${CPPUNIT_TEST_FILE}")

    configure_file("${CMAKE_CURRENT_LIST_DIR}/FindCPPUNIT-template-config.h"
        "${CPPUNIT_REPO_DIR}/include/cppunit/config-auto.h" @ONLY)
    target_include_directories(cppunit PRIVATE "${CPPUNIT_INCLUDE_DIRS}")
endif()

find_package_handle_standard_args(CPPUNIT
    REQUIRED_VARS CPPUNIT_LIBRARIES CPPUNIT_LINK_LIBRARIES)

if(CPPUNIT_FOUND)
    if(NOT TARGET cppunit)
        add_library(cppunit UNKNOWN IMPORTED)
    endif()
    set_target_properties(cppunit PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${CPPUNIT_INCLUDE_DIRS}"
        IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
        IMPORTED_LOCATION "${CPPUNIT_LINK_LIBRARIES}")
endif()

