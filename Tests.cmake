# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

option(TESTS "Compile and run unit tests" OFF)

# Required in most our generic main for tests
include(CheckFunctionExists)
include(Tools)

# CppUnit now requires a recent version of C++
# turn this always on
set_default(CMAKE_CXX_STANDARD 11)

if(TESTS)
    find_package(CPPUNIT REQUIRED)

    check_function_exists(getopt_long HAVE_GETOPT_LONG)

    if(NOT TARGET test-verbose)
        add_custom_target(test-verbose
            COMMAND ${CMAKE_CTEST_COMMAND} --verbose
            WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")
    endif()

    enable_testing()
endif()

