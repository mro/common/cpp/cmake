# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(FindPackageHandleStandardArgs)
include(xProject)
include(Tools)

set_default(EDGE_VERSION 3.1.4 ENV)

add_library(edge UNKNOWN IMPORTED)
if(EXTERNAL_DEPENDENCIES OR EXTERNAL_EDGE)
    message(STATUS "Using external edge...")
    xProject_cache_load(VARIABLES EDGE_INCLUDE_DIRS EDGE_LIBRARIES EDGE_EXECUTABLE)
    set_default(CPU "L867" ENV)
    # /acc/local/${CPU} should be in CMAKE_FIND_ROOT_PATH
    find_path(EDGE_INCLUDE_DIRS "edge/edge.h" PATHS
        "/acc/local/${CPU}/drv/edge/${EDGE_VERSION}/include"
        "/drv/edge/${EDGE_VERSION}/include")
    find_library(EDGE_LIBRARIES edge PATHS
        "/acc/local/${CPU}/drv/edge/${EDGE_VERSION}/lib"
        "/drv/edge/${EDGE_VERSION}/lib")
    find_program(EDGE_EXECUTABLE edge PATHS
        "/usr/local/edge"
        "/acc/local/share/edge/")
else()
    if(EDGE_VERSION VERSION_GREATER_EQUAL "4.0.0")
        set(EDGE_LIBRARIES "${CMAKE_BINARY_DIR}/instroot/lib/libedge.so")
        set(EDGE_INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/include")
    else()
        set(EDGE_LIBRARIES "${CMAKE_BINARY_DIR}/instroot/edge/${EDGE_VERSION}/lib/libedge.so")
        set(EDGE_INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/edge/${EDGE_VERSION}/include")
    endif()
endif()

xProject_Add(EDGE
    GIT_REPOSITORY "https://:@gitlab.cern.ch:8443/be-cem-edl/fec/utilities/edge.git"
    GIT_TAG "v${EDGE_VERSION}"
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX= -DCMAKE_INSTALL_LIBDIR=${CMAKE_INSTALL_LIBDIR}
    BUILD_COMMAND $(MAKE) CFLAGS+=-w
    UPDATE_COMMAND :
    INSTALL_COMMAND $(MAKE) install "DESTDIR=${CMAKE_BINARY_DIR}/instroot"
    LIBRARIES "${EDGE_LIBRARIES}"
    INCLUDE_DIRS "${EDGE_INCLUDE_DIRS}")

    add_dependencies(edge EDGE)

if(NOT EXTERNAL_EDGE)
    set(EDGE_EXECUTABLE "${CMAKE_BINARY_DIR}/instroot/edge/edge" CACHE PATH "edge executable")
    file(MAKE_DIRECTORY "${EDGE_INCLUDE_DIRS}")
endif()

find_package_handle_standard_args(EDGE
    REQUIRED_VARS EDGE_LIBRARIES EDGE_INCLUDE_DIRS EDGE_EXECUTABLE)

xproject_install(EDGE)

# FIXME remove when edge is properly linked with those
set(EDGE_LIBRARY ${EDGE_LIBRARIES})
list(APPEND EDGE_LIBRARIES rt pthread)

set_target_properties(edge PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${EDGE_INCLUDE_DIRS}"
    # FIXME remove when edge is properly linked with those
    INTERFACE_LINK_LIBRARIES "rt;pthread"
    IMPORTED_LINK_INTERFACE_LANGUAGES "C"
    IMPORTED_LOCATION "${EDGE_LIBRARY}")
