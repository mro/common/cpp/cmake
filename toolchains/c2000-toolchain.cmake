# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2024 CERN (home.cern)
# SPDX-Created: 2024-06-28T20:40:23
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

set(CMAKE_SYSTEM_NAME Generic)
set(CMAKE_SYSTEM_VERSION 1)
set(CMAKE_C_COMPILER_TARGET TI)
set(CMAKE_TRY_COMPILE_TARGET_TYPE STATIC_LIBRARY)

# program names
find_program(TI_CC cl2000 REQUIRED)
find_program(TI_STRIP strip2000 REQUIRED)
find_program(TI_OBJCOPY ofd2000 REQUIRED)
find_program(TI_OBJDUMP hex2000 REQUIRED)
find_program(TI_ASMDUMP dis2000 REQUIRED)
set(TI_LD ${TI_CC})

set(CMAKE_EXECUTABLE_SUFFIX ".out")

get_filename_component(TI_CGT_PATH "${TI_CC}" DIRECTORY)
get_filename_component(TI_CGT_PATH "${TI_CGT_PATH}" DIRECTORY)
if(NOT EXISTS "${TI_CGT_PATH}/include")
    message(ERROR "TI CGT include directory not found")
endif()
if(NOT EXISTS "${TI_CGT_PATH}/lib")
    message(ERROR "TI CGT lib directory not found")
endif()

set(CMAKE_C_COMPILER ${TI_CC} CACHE FILEPATH "TI C2000 C Compiler")
set(CMAKE_CXX_COMPILER ${TI_CC} CACHE FILEPATH "TI C2000 C++ Compiler")
set(CMAKE_ASM_COMPILER ${TI_CC} CACHE FILEPATH "TI C2000 Compiler")

if(NOT DEFINED CPU_FAMILY)
    set(CPU_FAMILY "f2837xd")
endif()
set(CPU_FAMILY "${CPU_FAMILY}" CACHE STRING "target CPU_FAMILY, default: f2837xd")

if(NOT DEFINED CPU)
    set(CPU "TMS320F28379D")
endif()
set(CPU "${CPU}" CACHE STRING "target CPU, default: TMS320F28379D")

set(CMAKE_EXE_LINKER_FLAGS_INIT "--rom_model --reread_libs --search_path=${TI_CGT_PATH}/lib")
# When symbols of libraries are referred to in ld script, but not used by binary:
# error #10068-D: no matching section
set(CMAKE_EXE_LINKER_FLAGS_INIT "${CMAKE_EXE_LINKER_FLAGS_INIT} --diag_suppress=10068")

string(TOUPPER "${CPU_FAMILY}" _CPU_FAMILY)
set(CMAKE_C_FLAGS_INIT "-v28 -mt --gen_func_subsections --display_error_number --emit_warnings_as_errors")
set(CMAKE_C_FLAGS_INIT "${CMAKE_C_FLAGS_INIT} --define=${_CPU_FAMILY}")
set(CMAKE_C_FLAGS_INIT "${CMAKE_C_FLAGS_INIT} --include_path=${TI_CGT_PATH}/include")
set(CMAKE_CXX_FLAGS_INIT "${CMAKE_C_FLAGS_INIT}")
set(CMAKE_C_FLAGS_INIT "${CMAKE_C_FLAGS_INIT} --c11")

unset(_CPU_FAMILY)

set(CMAKE_C_FLAGS_DEBUG_INIT "-g")
set(CMAKE_CXX_FLAGS_DEBUG_INIT "-g")

# Forces all sources to be relocated in RAM
function(ti_enable_ramfunc)
    add_compile_options("--ramfunc=on")
endfunction()

# Warning: default level is 0, do always enable it
function(ti_opt_level LEVEL)
    add_compile_options("--opt_level=${LEVEL}")
endfunction()

function(ti_prepare_executable TARGET_NAME)
    message(STATUS "Adding TI build steps for ${TARGET_NAME} ...")
    # Required by hex2000
    set_target_properties(${TARGET_NAME} PROPERTIES SUFFIX ".out")

    if(NOT CMAKE_BUILD_TYPE STREQUAL "Debug")
        add_custom_command(TARGET "${TARGET_NAME}" POST_BUILD
            COMMAND ${TI_STRIP} "$<TARGET_FILE:${TARGET_NAME}>"
            WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
    endif()

    add_custom_command(TARGET ${TARGET_NAME} POST_BUILD
        COMMAND ${TI_OBJDUMP} --memwidth=16 --romwidth=16 -i "$<TARGET_FILE:${TARGET_NAME}>" -o ${TARGET_NAME}.hex
        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
        BYPRODUCTS "${TARGET_NAME}.hex")

    add_custom_command(TARGET "${TARGET_NAME}" POST_BUILD
        COMMAND ${TI_ASMDUMP} "$<TARGET_FILE:${TARGET_NAME}>" "${TARGET_NAME}.asm"
        WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
        BYPRODUCTS "${TARGET_NAME}.asm")
endfunction()

function(c2000_clangd_extra)
    # C2000 compilers have exotic arguments, compile_commands.json will not be suitable for clangd
    # Generate a custom file
    set(CLANGD_CONFIG_EXTRA "-I${TI_CGT_PATH}/include" "-nobuiltininc")
    file(TOUCH ${CMAKE_CURRENT_LIST_DIR}/.cl2000.c)
    separate_arguments(PREPROC_CMD UNIX_COMMAND "${CMAKE_C_COMPILER} ${CMAKE_C_FLAGS_INIT} --preproc_macros=${CMAKE_CURRENT_LIST_DIR}/.cl2000.pp ${CMAKE_CURRENT_LIST_DIR}/.cl2000.c")
    execute_process(COMMAND ${PREPROC_CMD})
    file(STRINGS ${CMAKE_CURRENT_LIST_DIR}/.cl2000.pp PREPROC_ARGS)
    foreach(LINE IN LISTS PREPROC_ARGS)
        string(REGEX REPLACE "/[*].*[*]/" "" LINE "${LINE}")
        string(REGEX REPLACE "#define ([^ ]*) (.*[^ ])$" "-D\\1=\\2" LINE "${LINE}")
        string(STRIP "${LINE}" LINE)
        string(REGEX REPLACE "\"" "\\\\\"" LINE "${LINE}")
        list(APPEND CLANGD_CONFIG_EXTRA "${LINE}")
    endforeach()
    set(CLANGD_CONFIG_EXTRA "${CLANGD_CONFIG_EXTRA}" PARENT_SCOPE)
    set(CLANG_COMPLETE_EXTRA "${CLANGD_CONFIG_EXTRA}" PARENT_SCOPE)
    file(REMOVE ${CMAKE_CURRENT_LIST_DIR}/.cl2000.c ${CMAKE_CURRENT_LIST_DIR}/.cl2000.pp)
endfunction()

c2000_clangd_extra()

