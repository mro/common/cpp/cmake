# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(CMakeParseArguments)
include(ExternalProject)

include(ExtCache)

if(WIN32)
    set(CMAKE_INSTALL_BINDIR "bin" CACHE PATH "")
    set(CMAKE_INSTALL_LIBDIR "bin" CACHE PATH "")
    set(CMAKE_INSTALL_INCLUDEDIR "include" CACHE PATH "")
    set(CMAKE_INSTALL_DATADIR "data" CACHE PATH "")
else(WIN32)
    include(GNUInstallDirs)
endif(WIN32)

set_directory_properties(PROPERTIES
    EP_BASE "${CMAKE_BINARY_DIR}/external")

if(NOT GIT_EXE)
    find_package(Git)
    if(GIT_FOUND)
        set(GIT_EXE ${GIT_EXECUTABLE})
    endif(GIT_FOUND)
endif(NOT GIT_EXE)

function(xExternalProject_Add TARGET)
    cmake_parse_arguments(P "NOPREFETCH;AUTO_UPDATE;GIT_SUBMODULE" "GIT_REPOSITORY;GIT_TAG;LIBDIR;SOURCE_DIR" "INCLUDE_DIRS;LIBRARIES;CMAKE_CACHE_ARGS;CMAKE_ARGS;INSTALL_COMMAND;UPDATE_COMMAND;BUILD_COMMAND" "${ARGN}")
    string(TOLOWER "${TARGET}" TARGET_LOW)

    # Forward base args by default
    xProject_forward_args(${TARGET} CMAKE_ARGS CMAKE_BUILD_TYPE
        CMAKE_TOOLCHAIN_FILE CMAKE_CXX_STANDARD CMAKE_C_STANDARD)

    # Override variable from env or cmdline args: `<TARGET>_<VAR>`
    macro(arg_override)
        foreach(VAR ${ARGN})
            if(DEFINED ENV{${TARGET}_${VAR}})
                set(P_${VAR} $ENV{${TARGET}_${VAR}})
            elseif(DEFINED ${TARGET}_${VAR})
                set(P_${VAR} ${${TARGET}_${VAR}})
            endif()
        endforeach()
    endmacro()
    # call arg_override + append values from : `EXTRA_<TARGET>_<VAR>`
    macro(arg_list_override)
        arg_override(${ARGN})
        foreach(VAR ${ARGN})
            if(DEFINED ENV{EXTRA_${TARGET}_${VAR}})
                list(APPEN P_${VAR} $ENV{EXTRA_${TARGET}_${VAR}})
            endif()
            if(DEFINED EXTRA_${TARGET}_${VAR})
                list(APPEND P_${VAR} ${EXTRA_${TARGET}_${VAR}})
            endif()
        endforeach()
    endmacro()
    macro(arg_prepare)
        foreach(VAR ${ARGN})
            if(DEFINED P_${VAR})
                list(APPEND P_ARG "${VAR}" "${P_${VAR}}")
            elseif(${VAR} IN_LIST P_KEYWORDS_MISSING_VALUES)
                list(APPEND P_ARG "${VAR}" "")
            endif()
        endforeach()
    endmacro()

    # Overrides
    arg_override(GIT_REPOSITORY GIT_TAG SOURCE_DIR GIT_SUBMODULE)
    arg_list_override(CMAKE_CACHE_ARGS CMAKE_ARGS)

    if(NOT P_INCLUDE_DIRS)
        set(P_INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/${TARGET_LOW}/${CMAKE_INSTALL_INCLUDEDIR}")
    endif(NOT P_INCLUDE_DIRS)
    if(NOT P_LIBDIR)
        set(P_LIBDIR "${CMAKE_BINARY_DIR}/instroot/${TARGET_LOW}/${CMAKE_INSTALL_LIBDIR}")
    endif(NOT P_LIBDIR)

    if(NOT P_SOURCE_DIR)
        set(P_SOURCE_DIR "${CMAKE_SOURCE_DIR}/deps/${TARGET_LOW}")
    endif()
    list(APPEND P_CMAKE_CACHE_ARGS "-DEXT_CACHE:PATH=${CMAKE_BINARY_DIR}")

    # Git submodule support
    if(P_GIT_SUBMODULE)
        unset(P_GIT_REPOSITORY) # External project will try to clone otherwise
        if(NOT P_NOPREFETCH AND NOT EXISTS "${P_SOURCE_DIR}/.git")
            message(STATUS "Initializing git submodule: ${TARGET}")
            if(CMAKE_VERSION VERSION_LESS 3.19)
                execute_process(COMMAND ${GIT_EXE} "submodule" "update" "--init" "--recursive" "${P_SOURCE_DIR}"
                    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}")
            else()
                execute_process(COMMAND ${GIT_EXE} "submodule" "update" "--init" "--recursive" "${P_SOURCE_DIR}"
                    WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
                    ECHO_ERROR_VARIABLE ECHO_OUTPUT_VARIABLE
                    COMMAND_ERROR_IS_FATAL ANY)
            endif()
        endif()
    endif()

    arg_prepare(CMAKE_CACHE_ARGS CMAKE_ARGS INSTALL_COMMAND UPDATE_COMMAND BUILD_COMMAND
        GIT_REPOSITORY GIT_TAG)

    cmake_policy(PUSH)
    if(CMAKE_VERSION VERSION_GREATER_EQUAL "3.19")
        cmake_policy(SET CMP0114 NEW)
    endif()

    ExternalProject_Add("${TARGET}"
        ${P_ARG}
        SOURCE_DIR "${P_SOURCE_DIR}"
        ${P_UNPARSED_ARGUMENTS})

    cmake_policy(POP)

    get_property(stamp TARGET ${TARGET} PROPERTY _EP_STAMP_DIR)
    if(NOT P_AUTO_UPDATE)
        # Disable default update by creating stamp
        # Depends on same thing than update, but is ran after and does create the stamp
        # Workarround for CMake <= 2.8 on CC7
        ExternalProject_Add_Step(${TARGET} update-stamp
            COMMENT "Creating timestamp for update step"
            COMMAND touch "${stamp}/${TARGET}-update"
            COMMAND touch "${stamp}/${TARGET}-update-stamp"
            DEPENDEES update download
            DEPENDERS build)
    endif()
    ExternalProject_Add_Step(${TARGET} install-always
        COMMAND ${CMAKE_COMMAND} -E rm -f "${stamp}/${TARGET}-install"
        DEPENDEES install
        ALWAYS 1)

    set(${TARGET}_SOURCE_DIR "${P_SOURCE_DIR}" CACHE PATH "Source directory for ${TARGET} external-project" FORCE)
    set(${TARGET}_INCLUDE_DIRS "${P_INCLUDE_DIRS}" CACHE PATH "Include directory for ${TARGET} external-project" FORCE)
    set(${TARGET}_LIBRARIES "${P_LIBRARIES}" CACHE PATH "Libraries for ${TARGET} external-project" FORCE)
    set(${TARGET}_LIBDIR "${P_LIBDIR}" CACHE PATH "Link directory for ${TARGET} external-project" FORCE)

    if(NOT P_NOPREFETCH AND NOT P_GIT_SUBMODULE)
        get_property(stamp TARGET ${TARGET} PROPERTY _EP_STAMP_DIR)
        get_property(command TARGET ${TARGET} PROPERTY _EP_download_COMMAND)
        message(STATUS "Prefetching ${TARGET}")

        # Handling multi-commands
        set(P_SUB "")
        foreach(PART ${command})
            if(PART STREQUAL "COMMAND")
                execute_process(COMMAND ${P_SUB})
                set(P_SUB "")
            else()
                list(APPEND P_SUB "${PART}")
            endif()
        endforeach()
        if(P_SUB)
            execute_process(COMMAND ${P_SUB})
        endif()
        file(WRITE "${stamp}/${TARGET}-download" "")
    endif()
    xProject_register(${TARGET})
endfunction()

function(xProject_register TARGET)
    if(NOT TARGET xProject-download)
        add_custom_target(xProject-download)
    endif(NOT TARGET xProject-download)
    get_property(command TARGET ${TARGET} PROPERTY _EP_download_COMMAND)
    if(command)
        add_custom_command(TARGET xProject-download
            COMMAND ${command})
    endif()

    if(NOT TARGET xProject-update)
        add_custom_target(xProject-update)
    endif(NOT TARGET xProject-update)
    get_property(command TARGET ${TARGET} PROPERTY _EP_update_COMMAND)
    add_custom_command(TARGET xProject-update
        COMMAND ${CMAKE_COMMAND} -E echo "-- Updating ${TARGET}"
        COMMAND ${command})

    if(NOT TARGET xProject-build)
        add_custom_target(xProject-build)
    endif(NOT TARGET xProject-build)
    get_property(command TARGET ${TARGET} PROPERTY _EP_build_COMMAND)
    add_custom_command(TARGET xProject-build
        COMMAND ${CMAKE_COMMAND} -E echo "-- Building ${TARGET}"
        COMMAND ${command})
endfunction()

macro(xProject_Add TARGET)
    list(APPEND xProject_list ${TARGET})
    set(xProject_list "${xProject_list}" CACHE INTERNAL "" FORCE)

    if(EXT_CACHE)
        load_extern_cache(CACHE_DIR ${EXT_CACHE} VAR_LIST
            "${TARGET}_SOURCE_DIR"
            "${TARGET}_INCLUDE_DIRS"
            "${TARGET}_LIBRARIES"
            "${TARGET}_LIBDIR")
    endif(EXT_CACHE)

    if(EXTERNAL_DEPENDENCIES OR EXTERNAL_${TARGET})
        if(NOT ${TARGET}_INCLUDE_DIRS)
            message(FATAL_ERROR "External dependency not set, ${TARGET}_INCLUDE_DIRS missing")
        endif(NOT ${TARGET}_INCLUDE_DIRS)
        set(EXTERNAL_${TARGET} ON)
        add_custom_target(${TARGET})
    else(EXTERNAL_DEPENDENCIES OR EXTERNAL_${TARGET})
        xExternalProject_Add(${TARGET} ${ARGN})
    endif(EXTERNAL_DEPENDENCIES OR EXTERNAL_${TARGET})
endmacro()

# Install external project as part of current project
function(xProject_Install TARGET)
    if(NOT EXTERNAL_DEPENDENCIES AND NOT EXTERNAL_${TARGET})
        get_property(bindir TARGET ${TARGET} PROPERTY _EP_BINARY_DIR)
        set(OPTS "")
        if(CMAKE_INSTALL_PREFIX)
            set(OPTS "${OPTS} --prefix ${CMAKE_INSTALL_PREFIX}")
        endif()
        install(CODE "execute_process(COMMAND ${CMAKE_COMMAND} --install ${bindir} ${OPTS})")
    endif()
endfunction()

macro(xProject_Add_Step TARGET)
    ExternalProject_Add_Step(${TARGET} ${ARGN})
endmacro()

function(xProject_cache_load)
  if(EXT_CACHE)
    cmake_parse_arguments(P "" "" "VARIABLES" "${ARGN}")
    load_extern_cache(CACHE_DIR ${EXT_CACHE} VAR_LIST ${P_VARIABLES})
  endif(EXT_CACHE)
endfunction()

# @brief forward a target to external-projects
# @details creates a sub-target adding it as a dependency
# works mainly with GNU Make
function(xProject_forward_targets)
  cmake_parse_arguments(P "" "" "TARGETS;EXTERNAL_PROJECTS" "${ARGN}")

  foreach(target IN LISTS P_TARGETS)
    if(NOT TARGET ${target})
      add_custom_target(${target})
    endif()

    foreach(ep IN LISTS P_EXTERNAL_PROJECTS)
        # ExternalProject_Get_Property(${ep} BINARY_DIR)
        ExternalProject_Add_Step(${ep} ${target} DEPENDEES configure
            EXCLUDE_FROM_MAIN TRUE
            COMMAND ${CMAKE_COMMAND} --build <BINARY_DIR> --target ${target})
        ExternalProject_Add_StepTargets(${ep} ${target})

        add_dependencies(${target} ${ep}-${target})
    endforeach()
  endforeach()
endfunction()

function(xProject_forward_args)
    cmake_parse_arguments(P "" "" "CMAKE_ARGS;EXTERNAL_PROJECTS" "${ARGN}")

    foreach(ep IN LISTS P_EXTERNAL_PROJECTS P_UNPARSED_ARGUMENTS)
        foreach(arg IN LISTS P_CMAKE_ARGS)
            if(DEFINED ${arg})
                set(EXTRA_${ep}_CMAKE_ARGS "${EXTRA_${ep}_CMAKE_ARGS};-D${arg}=${${arg}}" PARENT_SCOPE)
            endif()
        endforeach()
    endforeach()
endfunction()
