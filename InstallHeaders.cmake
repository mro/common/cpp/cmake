# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(CMakeParseArguments)

# Install headers keeping subdirectory structure
function(install_headers)
    cmake_parse_arguments(INSTHDRS "" "DESTINATION" "" ${ARGN})
    foreach(file ${INSTHDRS_UNPARSED_ARGUMENTS})
        get_filename_component(dir ${file} DIRECTORY)
        install(FILES ${file} DESTINATION ${INSTHDRS_DESTINATION}/${dir} COMPONENT Devel)
    endforeach()
endfunction()
