# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(FindPackageHandleStandardArgs)
include(xProject)
include(Tools)

set_default(EDGE_VERSION "3.1.8")
set_default(FMC_MFE_VERSION "0.0.5-dev")
set_default(CPU "L867" ENV)

add_library(fmc_mfe UNKNOWN IMPORTED)
set(FMC_MFE_INTERFACE_LINK_LIBRARIES)

if(EXTERNAL_DEPENDENCIES OR EXTERNAL_FMC_MFE)
    xProject_cache_load(VARIABLES FMC_MFE_LIBRARIES FMC_MFE_INCLUDE_DIRS)
    # /acc/local/${CPU} should be in CMAKE_FIND_ROOT_PATH
    find_path(FMC_MFE_INCLUDE_DIRS "fmc_mfe/libfmc_mfe.h" PATHS
        "/acc/local/${CPU}/drv/edge/modules/fmc_mfe/${FMC_MFE_VERSION}/include"
        "/drv/edge/modules/fmc_mfe/${FMC_MFE_VERSION}/include")
    find_library(FMC_MFE_LIBRARIES fmc_mfe PATHS
        "/acc/local/${CPU}/drv/edge/modules/fmc_mfe/${FMC_MFE_VERSION}/lib"
        "/drv/edge/modules/fmc_mfe/${FMC_MFE_VERSION}/lib")
endif()

xProject_Add(FMC_MFE
    GIT_REPOSITORY "https://:@gitlab.cern.ch:8443/mro/controls/sambuca/edge/fmc_mfe.git"
    CMAKE_CACHE_ARGS "-DEXT_CACHE:path=${CMAKE_BINARY_DIR}"
    CMAKE_ARGS -DCMAKE_INSTALL_PREFIX= -DCMAKE_INSTALL_LIBDIR=${CMAKE_INSTALL_LIBDIR}
        -DEXTERNAL_EDGE=$<IF:$<OR:$<BOOL:${EDGE_FOUND}>,$<BOOL:${EXTERNAL_EDGE}>>,ON,OFF>
        -DEXTERNAL_CCUT=$<IF:$<OR:$<BOOL:${CCUT_FOUND}>,$<BOOL:${EXTERNAL_CCUT}>>,ON,OFF>
        -DEXTERNAL_GRBL_SIM=$<IF:$<OR:$<BOOL:${GRBL_SIM_FOUND}>,$<BOOL:${EXTERNAL_GRBL_SIM}>>,ON,OFF>
    INSTALL_COMMAND $(MAKE) install "DESTDIR=${CMAKE_BINARY_DIR}/instroot"
    LIBRARIES "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_LIBDIR}/libfmc_mfe.so"
    INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/include")
add_dependencies(fmc_mfe FMC_MFE)

if(EXTERNAL_FMC_MFE)
    set(EXTERNAL_EDGE ON)
    find_package(EDGE)
    find_package_handle_standard_args(FMC_MFE
        REQUIRED_VARS FMC_MFE_LIBRARIES FMC_MFE_INCLUDE_DIRS EDGE_INCLUDE_DIRS EDGE_LIBRARIES)
    list(APPEND FMC_MFE_INCLUDE_DIRS ${EDGE_INCLUDE_DIRS})
    list(APPEND FMC_MFE_INTERFACE_LINK_LIBRARIES ${EDGE_LIBRARIES})
else()
    find_package_handle_standard_args(FMC_MFE
        REQUIRED_VARS FMC_MFE_LIBRARIES FMC_MFE_INCLUDE_DIRS)

    file(MAKE_DIRECTORY "${FMC_MFE_INCLUDE_DIRS}")
    xproject_install(FMC_MFE)
    if(NOT EDGE_FOUND AND NOT EXTERNAL_EDGE)
        file(MAKE_DIRECTORY "${CMAKE_BINARY_DIR}/instroot/edge/${EDGE_VERSION}/include")
        if(EDGE_VERSION VERSION_GREATER_EQUAL "4.0.0")
            list(APPEND FMC_MFE_INTERFACE_LINK_LIBRARIES "${CMAKE_BINARY_DIR}/instroot/lib/libedge.so")
            list(APPEND FMC_MFE_INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/include")
        else()
            list(APPEND FMC_MFE_INTERFACE_LINK_LIBRARIES "${CMAKE_BINARY_DIR}/instroot/edge/${EDGE_VERSION}/lib/libedge.so")
            list(APPEND FMC_MFE_INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/edge/${EDGE_VERSION}/include")
        endif()
    endif()
endif()

set_target_properties(fmc_mfe PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES "${FMC_MFE_INCLUDE_DIRS}"
    INTERFACE_LINK_LIBRARIES "${FMC_MFE_INTERFACE_LINK_LIBRARIES}"
    IMPORTED_LOCATION "${FMC_MFE_LIBRARIES}")
