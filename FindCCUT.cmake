# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(FindPackageHandleStandardArgs)
include(xProject)
include(Tools)

if(EXTERNAL_DEPENDENCIES OR EXTERNAL_CCUT)
  message(STATUS "Using external libccut...")
  xProject_cache_load(VARIABLES
    CCUT_LIBRARIES CCUT_INCLUDE_DIRS
    LOGGER_LIBRARIES LOGGER_INCLUDE_DIRS)
  find_library(CCUT_LIBRARIES ccut)
  find_path(CCUT_INCLUDE_DIRS ccut/utils.hpp)
endif()

xProject_Add(CCUT
    GIT_REPOSITORY "https://gitlab.cern.ch/mro/common/libraries/libccut.git"
    CMAKE_ARGS -DBUILD_SHARED_LIBS=OFF -DCMAKE_INSTALL_PREFIX=
    INSTALL_COMMAND $(MAKE) install "DESTDIR=${CMAKE_BINARY_DIR}/instroot"
    LIBRARIES "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_LIBDIR}/libccut.a"
    INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_INCLUDEDIR}")

find_package_handle_standard_args(CCUT
  REQUIRED_VARS CCUT_LIBRARIES CCUT_INCLUDE_DIRS)

if(CCUT_FOUND)
  # Since we link in static we'd rather add that one
  find_package(posix REQUIRED COMPONENTS pthread)

  set(CCUT_INCLUDE_DIRS ${CCUT_INCLUDE_DIRS})
  set(CCUT_LIBRARIES ${CCUT_LIBRARIES})
  if(EXTERNAL_DEPENDENCIES OR EXTERNAL_CCUT)
    find_library(LOGGER_LIBRARIES logger)
    find_path(LOGGER_INCLUDE_DIRS logger/Logger.hpp)
    assert(LOGGER_LIBRARIES AND LOGGER_INCLUDE_DIRS MESSAGE "Failed to find liblogger")
  else()
    set(LOGGER_LIBRARIES "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_LIBDIR}/liblogger.a"
      CACHE PATH "Libraries for logger")
    set(LOGGER_INCLUDE_DIRS ${CCUT_INCLUDE_DIRS}
      CACHE PATH "Include directory for logger")
  endif()

  add_library(ccut::ccut UNKNOWN IMPORTED)
  file(MAKE_DIRECTORY "${CCUT_INCLUDE_DIRS}")
  set_target_properties(ccut::ccut PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${CCUT_INCLUDE_DIRS}"
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      IMPORTED_LOCATION "${CCUT_LIBRARIES}"
      IMPORTED_LINK_INTERFACE_LIBRARIES "${posix_pthread_LIBRARY}")
  add_dependencies(ccut::ccut CCUT)

  add_library(ccut::logger UNKNOWN IMPORTED)
  file(MAKE_DIRECTORY "${LOGGER_INCLUDE_DIRS}")
  set_target_properties(ccut::logger PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${LOGGER_INCLUDE_DIRS}"
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      IMPORTED_LOCATION "${LOGGER_LIBRARIES}"
      IMPORTED_LINK_INTERFACE_LIBRARIES "${posix_pthread_LIBRARY}")
  add_dependencies(ccut::logger CCUT)
  xproject_install(CCUT)
endif()
