# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(CMakeParseArguments)
include(FindPackageHandleStandardArgs)
include(xProject)
include(InstallSymlink)
include(CheckLibraryExists)

find_library(ATOMIC atomic)
if(ATOMIC_FOUND)
    message(STATUS "libatomic found")
endif(ATOMIC_FOUND)

check_library_exists(pthread pthread_setaffinity_np "pthread.h" HAVE_PTHREAD_SETAFFINITY_NP)

set(_CMAKE_ARGS
  -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE} -DCMAKE_INSTALL_PREFIX=
  -DCMAKE_POSITION_INDEPENDENT_CODE=ON
  -DCMAKE_INSTALL_LIBDIR=${CMAKE_INSTALL_LIBDIR}
  -DBUILD_SHARED_LIBS=OFF)

if(NOT HAVE_PTHREAD_SETAFFINITY_NP)
  list(APPEND _CMAKE_ARGS -DOATPP_COMPAT_BUILD_NO_SET_AFFINITY=ON)
endif()

set(OATPP_VERSION "1.3.0")
xProject_Add(OATPP
    GIT_REPOSITORY "https://gitlab.cern.ch/mro/third-party/oatpp/oatpp.git"
    GIT_TAG "master"
    CMAKE_ARGS ${_CMAKE_ARGS}
        -DOATPP_BUILD_TESTS=OFF -DOATPP_LINK_ATOMIC=$<IF:$<BOOL:${ATOMIC_FOUND}>,ON,OFF>
    INSTALL_COMMAND $(MAKE) install "DESTDIR=${CMAKE_BINARY_DIR}/instroot"
    LIBRARIES "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_LIBDIR}/oatpp-${OATPP_VERSION}/liboatpp.a"
    INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_INCLUDEDIR}/oatpp-${OATPP_VERSION}/oatpp")

find_package_handle_standard_args(OATPP
  REQUIRED_VARS OATPP_LIBRARIES OATPP_INCLUDE_DIRS)

if(OATPP_FOUND)
  # Since we link in static we'd rather add that one
  find_package(posix REQUIRED COMPONENTS pthread)

  add_library(oatpp UNKNOWN IMPORTED)
  xProject_Add(OATPPSwagger
      GIT_REPOSITORY "https://gitlab.cern.ch/mro/third-party/oatpp/oatpp-swagger.git"
      GIT_TAG "cern-1.0"
      SOURCE_DIR "${PROJECT_SOURCE_DIR}/deps/oatpp-swagger"
      CMAKE_ARGS ${_CMAKE_ARGS}
          -Doatpp_DIR=${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_LIBDIR}/cmake/oatpp-${OATPP_VERSION}
      INSTALL_COMMAND $(MAKE) install "DESTDIR=${CMAKE_BINARY_DIR}/instroot"
      LIBRARIES "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_LIBDIR}/oatpp-${OATPP_VERSION}/liboatpp-swagger.a"
      INCLUDE_DIRS "${CMAKE_BINARY_DIR}/instroot/${CMAKE_INSTALL_INCLUDEDIR}/oatpp-${OATPP_VERSION}/oatpp-swagger")
  add_dependencies(OATPPSwagger OATPP)

  file(MAKE_DIRECTORY "${OATPP_INCLUDE_DIRS}")
  file(MAKE_DIRECTORY "${OATPPSwagger_INCLUDE_DIRS}")
  set_target_properties(oatpp PROPERTIES
      INTERFACE_INCLUDE_DIRECTORIES "${OATPP_INCLUDE_DIRS};${OATPPSwagger_INCLUDE_DIRS}"
      IMPORTED_LINK_INTERFACE_LANGUAGES "CXX"
      IMPORTED_LOCATION "${OATPPSwagger_LIBRARIES}"
      IMPORTED_LINK_INTERFACE_LIBRARIES "${OATPP_LIBRARIES};${posix_pthread_LIBRARY}")
  add_dependencies(oatpp OATPP OATPPSwagger)

  add_executable(oatpp_mkbin EXCLUDE_FROM_ALL ${CMAKE_CURRENT_LIST_DIR}/oatpp_mkbin.c)
  function(OATPP_MKBIN)
    cmake_parse_arguments(P "" "TARGET;INFILE;OUTFILE;NAME" "FILES" "${ARGN}")
    if(P_TARGET AND NOT TARGET ${P_TARGET})
      add_custom_target(${P_TARGET})
      set_target_properties(${P_TARGET} PROPERTIES
        SOURCES ""
        INTERFACE_INCLUDE_DIRECTORIES ${CMAKE_CURRENT_BINARY_DIR})
    endif()

    if(P_INFILE)
      get_filename_component(FILENAME ${P_INFILE} NAME)
      if(NOT P_OUTFILE)
        set(P_OUTFILE "${FILENAME}.cpp")
      endif()
      if(NOT P_NAME)
        string(MAKE_C_IDENTIFIER ${FILENAME} P_NAME)
      endif()
      add_custom_command(OUTPUT ${P_OUTFILE}
        COMMAND ${CMAKE_BINARY_DIR}/oatpp_mkbin ${P_INFILE} ${P_OUTFILE} ${P_NAME}
        MAIN_DEPENDENCY ${P_INFILE} DEPENDS oatpp_mkbin)
      if(P_TARGET)
        get_target_property(SOURCES ${P_TARGET} SOURCES)
        set_target_properties(${P_TARGET} PROPERTIES SOURCES "${SOURCES};${P_OUTFILE}")
      endif()
    endif()

    foreach(F IN LISTS P_FILES)
      if(P_TARGET)
        oatpp_mkbin(TARGET ${P_TARGET} INFILE ${F})
      else()
        oatpp_mkbin(INFILE ${F})
      endif()
    endforeach()
  endfunction()

  set(OATPP_SWAGGER_RESOURCES
    ${PROJECT_SOURCE_DIR}/deps/oatpp-swagger/res/favicon-16x16.png
    ${PROJECT_SOURCE_DIR}/deps/oatpp-swagger/res/favicon-32x32.png
    ${PROJECT_SOURCE_DIR}/deps/oatpp-swagger/res/index.html
    ${PROJECT_SOURCE_DIR}/deps/oatpp-swagger/res/oauth2-redirect.html
    ${PROJECT_SOURCE_DIR}/deps/oatpp-swagger/res/swagger-ui-bundle.js
    ${PROJECT_SOURCE_DIR}/deps/oatpp-swagger/res/swagger-ui-es-bundle-core.js
    ${PROJECT_SOURCE_DIR}/deps/oatpp-swagger/res/swagger-ui-es-bundle.js
    ${PROJECT_SOURCE_DIR}/deps/oatpp-swagger/res/swagger-ui-standalone-preset.js
    ${PROJECT_SOURCE_DIR}/deps/oatpp-swagger/res/swagger-ui.css
    ${PROJECT_SOURCE_DIR}/deps/oatpp-swagger/res/swagger-ui.js CACHE INTERNAL "swagger resources list")

    xproject_install(OATPP)
    xproject_install(OATPPSwagger)
    install_symlink("oatpp-${OATPP_VERSION}/oatpp/oatpp" "${CMAKE_INSTALL_INCLUDEDIR}/oatpp")
    install_symlink("oatpp-${OATPP_VERSION}/oatpp-swagger/oatpp-swagger" "${CMAKE_INSTALL_INCLUDEDIR}/oatpp-swagger")
endif()

unset(OATPP_CMAKE_ARGS)
