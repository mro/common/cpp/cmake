# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

function(install_symlink SOURCE DESTINATION)
    get_filename_component(_installdir ${DESTINATION} PATH)
    get_filename_component(_filename ${DESTINATION} NAME)
    set(_tempfile "${CMAKE_CURRENT_BINARY_DIR}/${_filename}.install_symlink")

    install(CODE "
        message(STATUS \"Creating symlink: ${DESTINATION}\")
        execute_process(COMMAND ${CMAKE_COMMAND} -E create_symlink ${SOURCE} ${_tempfile})
    ")
    install(FILES ${_tempfile} DESTINATION ${_installdir} RENAME ${_filename})
    install(CODE "file(REMOVE ${_tempfile})")
endfunction()
