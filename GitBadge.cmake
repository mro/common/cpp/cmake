
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# Create a git badge

if(NOT CMAKE_SCRIPT_MODE_FILE)
  add_custom_target(git-badge DEPENDS ${CMAKE_BINARY_DIR}/git_badge.svg)
  add_custom_command(
      OUTPUT ${CMAKE_BINARY_DIR}/git_badge.svg
      COMMAND ${CMAKE_COMMAND} -DGIT_EXE="${GIT_EXE}"
        -DGIT_WORKDIR="${GIT_WORKDIR}" -P "${CMAKE_CURRENT_LIST_FILE}"
      DEPENDS ${CMAKE_SOURCE_DIR}/.git)
else()
  execute_process(
      COMMAND ${GIT_EXE} "describe" "--tags" "--always" "HEAD"
      WORKING_DIRECTORY "${GIT_WORKDIR}"
      OUTPUT_STRIP_TRAILING_WHITESPACE
      OUTPUT_VARIABLE GIT_DESCRIBE)

  message(STATUS "Git describe: ${GIT_DESCRIBE}")
  string(REPLACE "-" "--" GIT_BADGE "${GIT_DESCRIBE}")
  file(DOWNLOAD "https://img.shields.io/badge/version-${GIT_BADGE}-informational.svg" "${CMAKE_CURRENT_BINARY_DIR}/git_badge.svg")
  message(STATUS "Git badge generated !")
endif()
