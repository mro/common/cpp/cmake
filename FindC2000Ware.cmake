# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2024 CERN (home.cern)
# SPDX-Created: 2024-06-28T17:34:50
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# This module includes the following components:
# - driverlib : provides driverlib target
# - iqmath : provides C2000Ware::iqmath and C2000Ware::iqmath::fpu targets
# - uniflash : enables `add_flash_target` function
# - fastrts : enables Fast Run-Time-Support Library support (optimized FP maths functions)
# - flashapi : enables F021 Flash Application Programming Interface support
#
# This modules generates the following targets:
# - C2000Ware::driverlib : to use driverlib
# - C2000Ware::iqmath : to use iqmath library
# - C2000Ware::iqmath::fpu : to use iqmath with FPU support
# - C2000Ware::fpu : to use fpu without iqmath
# - C2000Ware::fastrts : to use fastrts library
# - C2000Ware::flashapi : to use flashapi library
#
# This module provides the following functions:
# - add_cla_sources : compile code for CLA co-processor
# - add_flash_target : add rule to flash a binary on target
#


include(CMakeParseArguments)
include(FindPackageHandleStandardArgs)
include(${CMAKE_CURRENT_LIST_DIR}/Tools.cmake)

set_default(C2000Ware_DIR "/opt/ti/c2000/C2000Ware_5_02_00_00")

if(EXISTS "${C2000Ware_DIR}")
    set(C2000Ware_DIR "${C2000Ware_DIR}" CACHE PATH "TI C2000Ware directory")
    list(APPEND CMAKE_FIND_ROOT_PATH "${C2000Ware_DIR}")
else()
    unset(C2000Ware_DIR)
endif()

assert(DEFINED CPU_FAMILY)
string(TOUPPER "${CPU_FAMILY}" _CPU_FAMILY_UPPER)
string(REPLACE "X" "x" _CPU_FAMILY_UPPER "${_CPU_FAMILY_UPPER}")

# Find IQMATH module
find_path(C2000Ware_iqmath_INCLUDE_DIR IQmathLib.h PATHS "/libraries/math/IQmath/c28/include")
find_file(C2000Ware_iqmath_LIBRARY IQmath.lib PATHS "/libraries/math/IQmath/c28/lib")
find_file(C2000Ware_iqmath_FPU_LIBRARY IQmath_fpu32.lib PATHS "/libraries/math/IQmath/c28/lib")

assert(C2000Ware_iqmath_INCLUDE_DIR OR NOT C2000Ware_FIND_REQUIRED_iqmath)
assert(C2000Ware_iqmath_LIBRARY OR NOT C2000Ware_FIND_REQUIRED_iqmath)
assert(C2000Ware_iqmath_FPU_LIBRARY OR NOT C2000Ware_FIND_REQUIRED_iqmath)
if(C2000Ware_iqmath_INCLUDE_DIR AND C2000Ware_iqmath_LIBRARY)
    set(C2000Ware_iqmath_FOUND 1)
    add_library(C2000Ware::iqmath UNKNOWN IMPORTED)
    target_compile_definitions(C2000Ware::iqmath INTERFACE "MATH_TYPE=IQ_MATH")
    set_target_properties(C2000Ware::iqmath PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${C2000Ware_iqmath_INCLUDE_DIR}"
        IMPORTED_LINK_INTERFACE_LANGUAGES "C"
        IMPORTED_LOCATION "${C2000Ware_iqmath_LIBRARY}")
endif()
if(C2000Ware_iqmath_INCLUDE_DIR AND C2000Ware_iqmath_FPU_LIBRARY)
    set(C2000Ware_iqmath_FOUND 1)
    add_library(C2000Ware::iqmath::fpu UNKNOWN IMPORTED)
    target_compile_options(C2000Ware::iqmath::fpu INTERFACE
        "SHELL:--float_support=fpu32" "SHELL:--fp_reassoc=on" "SHELL:--fp_mode=relaxed")
    target_compile_definitions(C2000Ware::iqmath::fpu INTERFACE "MATH_TYPE=IQ_MATH")
    set_target_properties(C2000Ware::iqmath::fpu PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${C2000Ware_iqmath_INCLUDE_DIR}"
        IMPORTED_LINK_INTERFACE_LANGUAGES "C"
        IMPORTED_LOCATION "${C2000Ware_iqmath_FPU_LIBRARY}")
endif()

# Raw FPU support
add_library(C2000Ware::fpu INTERFACE IMPORTED)
target_compile_options(C2000Ware::fpu INTERFACE
    "SHELL:--float_support=fpu32" "SHELL:--fp_reassoc=on" "SHELL:--fp_mode=relaxed")
target_compile_definitions(C2000Ware::fpu INTERFACE "MATH_TYPE=FLOAT_MATH")

# FastRTS library
find_path(C2000Ware_fastrts_INCLUDE_DIR C28x_FPU_FastRTS.h PATHS "/libraries/math/FPUfastRTS/c28/include")
find_path(C2000Ware_fastrts_LIBRARY_DIR rts2800_fpu32_fast_supplement.lib PATHS "/libraries/math/FPUfastRTS/c28/lib")
assert(C2000Ware_fastrts_INCLUDE_DIR OR NOT C2000Ware_FIND_REQUIRED_fastrts)
assert(C2000Ware_fastrts_LIBRARY_DIR OR NOT C2000Ware_FIND_REQUIRED_fastrts)
if(C2000Ware_fastrts_LIBRARY_DIR AND C2000Ware_fastrts_INCLUDE_DIR)
    set(C2000Ware_fastrts_FOUND 1)
    set(C2000Ware_fastrts_LIBRARY "${C2000Ware_fastrts_LIBRARY_DIR}/rts2800_fpu32_fast_supplement.lib"
        CACHE FILEPATH "C2000Ware Fast Run Time Support Library")
    add_library(C2000Ware::fastrts UNKNOWN IMPORTED)
    set_target_properties(C2000Ware::fastrts PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${C2000Ware_fastrts_INCLUDE_DIR}"
        IMPORTED_LINK_INTERFACE_LANGUAGES "C"
        IMPORTED_LOCATION "${C2000Ware_fastrts_LIBRARY}")
    target_compile_options(C2000Ware::fastrts INTERFACE "SHELL:--tmu_support=tmu0")
    target_link_options(C2000Ware::fastrts INTERFACE
        "--search_path=${C2000Ware_fastrts_LIBRARY_DIR}"
        "--define=HAVE_FASTRTS")
    target_compile_definitions(C2000Ware::fastrts INTERFACE "HAVE_FASTRTS")
endif()

# Find flashapi F021 library
find_path(C2000Ware_flashapi_INCLUDE_DIR F021.h PATHS "/libraries/flash_api/${CPU_FAMILY}/include")
find_file(C2000Ware_flashapi_LIBRARY F021_API.lib
    NAMES F021_API_${_CPU_FAMILY_UPPER}_FPU32.lib F021_API_${_CPU_FAMILY_UPPER}.lib
    PATHS "/libraries/flash_api/${CPU_FAMILY}/lib")
assert(C2000Ware_flashapi_INCLUDE_DIR OR NOT C2000Ware_FIND_REQUIRED_flashapi)
assert(C2000Ware_flashapi_LIBRARY OR NOT C2000Ware_FIND_REQUIRED_flashapi)
if(C2000Ware_flashapi_LIBRARY AND C2000Ware_flashapi_INCLUDE_DIR)
    set(C2000Ware_flashapi_FOUND 1)
    add_library(C2000Ware::flashapi UNKNOWN IMPORTED)
    get_filename_component(C2000Ware_flashapi_LIBRARY_DIR ${C2000Ware_flashapi_LIBRARY} DIRECTORY CACHE)
    set_target_properties(C2000Ware::flashapi PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${C2000Ware_flashapi_INCLUDE_DIR}"
        IMPORTED_LINK_INTERFACE_LANGUAGES "C"
        IMPORTED_LOCATION "${C2000Ware_flashapi_LIBRARY}")
    target_link_options(C2000Ware::flashapi INTERFACE
        "--search_path=${C2000Ware_flashapi_LIBRARY_DIR}"
        "--define=HAVE_FLASHAPI")
    target_compile_definitions(C2000Ware::flashapi INTERFACE "HAVE_FLASHAPI")

    find_file(C2000Ware_flashapi_UserDefinedFunctions
        NAMES "Fapi_UserDefinedFunctions.c"
        PATHS "/libraries/flash_api/${CPU_FAMILY}/source")
    if(C2000Ware_flashapi_UserDefinedFunctions AND NOT C2000Ware_flashapi_UserDefinedFunctions_DISABLE)
        target_sources(C2000Ware::flashapi INTERFACE ${C2000Ware_flashapi_UserDefinedFunctions})
    endif()
endif()

# Create interface library
add_library(C2000Ware INTERFACE IMPORTED)

find_file(C2000WARE_CSBRANCH
    NAMES "${CPU_FAMILY}_CodeStartBranch.asm" "${_CPU_FAMILY_UPPER}_CodeStartBranch.asm"
    PATHS "/device_support/${CPU_FAMILY}/common/source" REQUIRED)
unset(_CPU_FAMILY_UPPER)
target_sources(C2000Ware INTERFACE ${C2000WARE_CSBRANCH})

find_path(C2000Ware_driverlib_INCLUDE_DIR driverlib/sysctl.h
    PATHS "/driverlib/${CPU_FAMILY}")
if(C2000Ware_driverlib_INCLUDE_DIR)
    set(C2000Ware_driverlib_FOUND 1)
    add_library(C2000Ware::driverlib INTERFACE IMPORTED)
    set_target_properties(C2000Ware::driverlib PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${C2000Ware_driverlib_INCLUDE_DIR}"
        IMPORTED_LINK_INTERFACE_LANGUAGES "C")
    file(GLOB C2000Ware_driverlib_SRC "${C2000Ware_driverlib_INCLUDE_DIR}/driverlib/*.c")
    target_sources(C2000Ware::driverlib INTERFACE ${C2000Ware_driverlib_SRC})
    target_compile_options(C2000Ware::driverlib INTERFACE
        # variable was set but never used
        "SHELL:--diag_suppress=552"
        # variable was declared but never referenced
        "SHELL:--diag_suppress=179"
        )
endif()

find_program(UNIFLASH_EXE
    NAMES dslite.sh dslite PATHS /opt/ti/uniflash
    DOC "Uniflash firmware loader")
if(UNIFLASH_EXE)
    set(C2000Ware_uniflash_FOUND 1)
endif()

function(get_link_libraries OUT TARGET_NAME)
    get_target_property(linklibs "${TARGET_NAME}" LINK_LIBRARIES)
    get_target_property(ilinklibs "${TARGET_NAME}" INTERFACE_LINK_LIBRARIES)
    list(APPEND linklibs ${ilinklibs})
    list(REMOVE_DUPLICATES linklibs)

    foreach(link IN LISTS linklibs)
        if(TARGET ${link})
            list(APPEND "${OUT}" ${link})
            get_link_libraries(sub ${link})
            list(APPEND "${OUT}" ${sub})
        endif()
    endforeach()

    list(REMOVE_DUPLICATES "${OUT}")
    set("${OUT}" "${${OUT}}" PARENT_SCOPE)
endfunction()

function(get_recursive_target_property OUT target property)
    get_target_property(VALUE ${target} ${property})
    if(VALUE)
        list(APPEND ${OUT} ${VALUE})
    endif()


    get_link_libraries(linklibs ${target})
    foreach(link IN LISTS linklibs)
        if(TARGET ${link})
            get_target_property(VALUE ${link} ${property})
            if(VALUE)
                list(APPEND ${OUT} ${VALUE})
            endif()
        endif()
    endforeach()

    list(REMOVE_DUPLICATES "${OUT}")
    set("${OUT}" "${${OUT}}" PARENT_SCOPE)
endfunction()


function(add_flash_target TARGET_NAME)
    cmake_parse_arguments(P "NO_RESET" "CCXML" "" "${ARGN}")

    set(CMD "${UNIFLASH_EXE}" "-v" "-f" $<TARGET_FILE:${TARGET_NAME}>)

    assert(P_CCXML MESSAGE "CCXML configuration file required")
    if(NOT EXISTS "${P_CCXML}")
        if(EXISTS "${CMAKE_CURRENT_SOURCE_DIR}/${P_CCXML}")
            set(P_CCXML "${CMAKE_CURRENT_SOURCE_DIR}/${P_CCXML}")
        elseif(EXISTS "${PROJECT_SOURCE_DIR}/${P_CCXML}")
            set(P_CCXML "${PROJECT_SOURCE_DIR}/${P_CCXML}")
        else()
            message(SEND_ERROR "CCXML file not found: ${P_CCXML}")
        endif()
    endif()
    list(APPEND CMD "--config=${P_CCXML}")

    if(NOT P_NO_RESET)
        list(APPEND CMD "-r" "0")
    endif()
    get_recursive_target_property(COMPILE_DEFINITIONS "${TARGET_NAME}" COMPILE_DEFINITIONS)
    get_recursive_target_property(COMPILE_DEFINITIONS "${TARGET_NAME}" INTERFACE_COMPILE_DEFINITIONS)
    if(CPU1 IN_LIST COMPILE_DEFINITIONS)
        list(APPEND CMD "-n" "0")
    elseif(CPU2 IN_LIST COMPILE_DEFINITIONS)
        list(APPEND CMD "-n" "1")
    else()
        message(SEND_ERROR "no CPU set on target ${TARGET_NAME}, please add CPU1/CPU2 compile_definition")
    endif()

    add_custom_target(${TARGET_NAME}_flash ${CMD}
        DEPENDS ${TARGET_NAME})
endfunction()

function(add_cla_sources TARGET_NAME)
    cmake_parse_arguments(P "" "CLA" "SOURCES" "${ARGN}")
    set_default(P_CLA "cla1")

    target_sources(${TARGET_NAME} PRIVATE ${P_SOURCES} ${P_UNPARSED_ARGUMENTS})
    foreach(src IN LISTS P_SOURCES P_UNPARSED_ARGUMENTS)
        set_source_files_properties(${src} PROPERTIES
            LANGUAGE C
            COMPILE_OPTIONS "--cla_support=${P_CLA};--cla_default")
    endforeach()
    target_compile_definitions(${TARGET_NAME} PUBLIC CLA_C)
    target_link_options(${TARGET_NAME} PUBLIC --define=CLA_C --cla_support=${P_CLA})
endfunction()

find_package_handle_standard_args(C2000Ware
    REQUIRED_VARS C2000Ware_DIR
    HANDLE_COMPONENTS)
