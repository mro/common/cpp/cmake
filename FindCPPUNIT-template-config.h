#ifndef _INCLUDE_CPPUNIT_CONFIG_AUTO_H
#define _INCLUDE_CPPUNIT_CONFIG_AUTO_H 1

/* include/cppunit/config-auto.h. Generated automatically at end of configure. */
/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* define if library uses std::string::compare(string,pos,n) */
/* #undef FUNC_STRING_COMPARE_STRING_FIRST */

/* define if the library defines strstream */
#ifndef CPPUNIT_HAVE_CLASS_STRSTREAM
#cmakedefine01 CPPUNIT_HAVE_CLASS_STRSTREAM
#endif

/* Define to 1 if you have the <cmath> header file. */
#ifndef CPPUNIT_HAVE_CMATH
#cmakedefine01 CPPUNIT_HAVE_CMATH
#endif

/* define if the compiler supports basic C++11 syntax */
#ifndef CPPUNIT_HAVE_CXX11
#define CPPUNIT_HAVE_CXX11 1
#endif

/* Define if you have the GNU dld library. */
/* #undef HAVE_DLD */

/* Define to 1 if you have the `dlerror' function. */
#ifndef CPPUNIT_HAVE_DLERROR
#cmakedefine01 CPPUNIT_HAVE_DLERROR
#endif

/* Define to 1 if you have the <dlfcn.h> header file. */
#ifndef CPPUNIT_HAVE_DLFCN_H
#cmakedefine01 CPPUNIT_HAVE_DLFCN_H
#endif

/* define if the compiler supports GCC C++ ABI name demangling */
#ifndef CPPUNIT_HAVE_GCC_ABI_DEMANGLE
#define CPPUNIT_HAVE_GCC_ABI_DEMANGLE 1
#endif

/* Define to 1 if you have the <inttypes.h> header file. */
#ifndef CPPUNIT_HAVE_INTTYPES_H
#cmakedefine01 CPPUNIT_HAVE_INTTYPES_H
#endif

/* Define if you have the libdl library or equivalent. */
#ifndef CPPUNIT_HAVE_LIBDL
#cmakedefine01 CPPUNIT_HAVE_LIBDL
#endif

/* Define to 1 if you have the <memory.h> header file. */
#ifndef CPPUNIT_HAVE_MEMORY_H
#cmakedefine01 CPPUNIT_HAVE_MEMORY_H
#endif

/* define if the compiler implements namespaces */
#ifndef CPPUNIT_HAVE_NAMESPACES
#cmakedefine01 CPPUNIT_HAVE_NAMESPACES
#endif

/* Define if you have the shl_load function. */
/* #undef HAVE_SHL_LOAD */

/* define if the compiler has stringstream */
#ifndef CPPUNIT_HAVE_SSTREAM
#cmakedefine01 CPPUNIT_HAVE_SSTREAM
#endif

/* Define to 1 if you have the <stdint.h> header file. */
#ifndef CPPUNIT_HAVE_STDINT_H
#define CPPUNIT_HAVE_STDINT_H 1
#endif

/* Define to 1 if you have the <stdlib.h> header file. */
#ifndef CPPUNIT_HAVE_STDLIB_H
#define CPPUNIT_HAVE_STDLIB_H 1
#endif

/* Define to 1 if you have the <strings.h> header file. */
#ifndef CPPUNIT_HAVE_STRINGS_H
#define CPPUNIT_HAVE_STRINGS_H 1
#endif

/* Define to 1 if you have the <string.h> header file. */
#ifndef CPPUNIT_HAVE_STRING_H
#define CPPUNIT_HAVE_STRING_H 1
#endif

/* Define to 1 if you have the <strstream> header file. */
#ifndef CPPUNIT_HAVE_STRSTREAM
#cmakedefine01 CPPUNIT_HAVE_STRSTREAM
#endif

/* Define to 1 if you have the <sys/stat.h> header file. */
#ifndef CPPUNIT_HAVE_SYS_STAT_H
#define CPPUNIT_HAVE_SYS_STAT_H 1
#endif

/* Define to 1 if you have the <sys/types.h> header file. */
#ifndef CPPUNIT_HAVE_SYS_TYPES_H
#define CPPUNIT_HAVE_SYS_TYPES_H 1
#endif

/* Define to 1 if you have the <unistd.h> header file. */
#ifndef CPPUNIT_HAVE_UNISTD_H
#define CPPUNIT_HAVE_UNISTD_H 1
#endif

/* Define to the sub-directory where libtool stores uninstalled libraries. */
#ifndef CPPUNIT_LT_OBJDIR
#define CPPUNIT_LT_OBJDIR ".libs/"
#endif

/* Disable debugging information */
#ifndef CPPUNIT_NDEBUG
#define CPPUNIT_NDEBUG /**/
#endif

/* Name of package */
#ifndef CPPUNIT_PACKAGE
#define CPPUNIT_PACKAGE "cppunit"
#endif

/* Define to the address where bug reports for this package should be sent. */
#ifndef CPPUNIT_PACKAGE_BUGREPORT
#define CPPUNIT_PACKAGE_BUGREPORT ""
#endif

/* Define to the full name of this package. */
#ifndef CPPUNIT_PACKAGE_NAME
#define CPPUNIT_PACKAGE_NAME "cppunit"
#endif

/* Define to the full name and version of this package. */
#ifndef CPPUNIT_PACKAGE_STRING
#define CPPUNIT_PACKAGE_STRING ("cppunit " CPPUNIT_VERSION)
#endif

/* Define to the one symbol short name of this package. */
#ifndef CPPUNIT_PACKAGE_TARNAME
#define CPPUNIT_PACKAGE_TARNAME "cppunit"
#endif

/* Define to the home page for this package. */
#ifndef CPPUNIT_PACKAGE_URL
#define CPPUNIT_PACKAGE_URL ""
#endif

/* defined in Portability.h */
#ifndef CPPUNIT_PACKAGE_VERSION
#define CPPUNIT_PACKAGE_VERSION CPPUNIT_VERSION
#endif

/* Define to 1 if you have the ANSI C header files. */
/* #undef STDC_HEADERS */


/* once: _INCLUDE_CPPUNIT_CONFIG_AUTO_H */
#endif