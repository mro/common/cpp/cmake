# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(FindPackageHandleStandardArgs)
include(xProject)

if(EXTERNAL_DEPENDENCIES OR EXTERNAL_GRBL_GPIOD)
    message(STATUS "Using external grbl-gpiod")
    xProject_cache_load(VARIABLES GRBL_GPIOD_PATH)
    find_program(GRBL_GPIOD_EXE grbl-gpiod PATHS ${GRBL_GPIOD_PATH})
endif()

xProject_Add(GRBL_GPIOD
    GIT_REPOSITORY "https://gitlab.cern.ch/mro/third-party/grbl/grbl-gpiod.git"
    # Require build with 8 axis, supress all warnings, disable gpiod lib
    CMAKE_ARGS
        -DCMAKE_INSTALL_PREFIX= -DCMAKE_C_FLAGS= -DN_AXIS=8 -DLIBGPIOD=OFF
    INSTALL_COMMAND $(MAKE) install "DESTDIR=${CMAKE_BINARY_DIR}/instroot")

if(NOT GRBL_GPIOD_EXE AND NOT EXTERNAL_GRBL_GPIOD)
    set(GRBL_GPIOD_EXE "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_BINDIR}/grbl-gpiod"
        CACHE PATH "grbl-gpiod exe")
endif()

find_package_handle_standard_args(GRBL_GPIOD REQUIRED_VARS GRBL_GPIOD_EXE)

xproject_install(GRBL_GPIOD)
