# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

if(CMAKE_SCRIPT_MODE_FILE)

    # Create a linter badge
    execute_process(
        COMMAND "${CMAKE_MAKE_PROGRAM}" lint
        ERROR_VARIABLE LINT_OUT)

    string(REGEX REPLACE "[^\n]+" "" LINT_COUNT "${LINT_OUT}")
    string(LENGTH "${LINT_COUNT}" LINT_COUNT)

    if(LINT_COUNT EQUAL 0)
        set(BADGE_COLOR "limegreen")
    elseif(LINT_COUNT LESS 6)
        set(BADGE_COLOR "gold")
    elseif(LINT_COUNT LESS 11)
        set(BADGE_COLOR "darkorange")
    else()
        set(BADGE_COLOR "tomato")
    endif()

    configure_file(
        "${CMAKE_CURRENT_LIST_DIR}/LintBadge-template.svg"
        "${CMAKE_CURRENT_BINARY_DIR}/lint_badge.svg" @ONLY)
    message(STATUS "Lint badge generated !")

elseif(NOT TARGET lint-badge)

    add_custom_target(lint-badge
        COMMAND ${CMAKE_COMMAND}
        -DCMAKE_MAKE_PROGRAM="${CMAKE_MAKE_PROGRAM}"
        -P "${CMAKE_CURRENT_LIST_FILE}"
        WORKING_DIRECTORY "${PROJECT_BINARY_DIR}")

endif()