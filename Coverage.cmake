# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

option(COVERAGE "Enable code coverage" OFF)

# Configurable options:
#   COVERAGE_SRCDIRS: directories filter for coverage information, default: src
#   LCOV_OPTIONS: options for lcov

include(Tools)

if(COVERAGE)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")
    set(CMAKE_C_FLAGS "${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage")

    set_default(COVERAGE_SRCDIRS "src")
    find_program(LCOV_EXE NAMES lcov
        PATHS ${LCOV_PATH} ENV PATH $ENV{LCOV_PATH}
        CMAKE_FIND_ROOT_PATH_BOTH)
    if(LCOV_EXE)
        message(STATUS "Lcov found")
        add_custom_target(lcov
            COMMAND ${CMAKE_COMMAND}
                "-DSRCDIRS=\"${COVERAGE_SRCDIRS}\""
                "-DLCOV_OPTIONS=\"${LCOV_OPTIONS}\""
                -P "${CMAKE_CURRENT_LIST_DIR}/LintLCov.cmake"
            WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")
        add_custom_target(lcov-result
            COMMAND ${CMAKE_COMMAND} -DMODE="result"
                -P "${CMAKE_CURRENT_LIST_DIR}/LintLCov.cmake"
            WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
            DEPENDS lcov)
    endif(LCOV_EXE)
    add_custom_target(cov-clean
        COMMAND ${CMAKE_COMMAND} -DMODE="clean"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintLCov.cmake"
        WORKING_DIRECTORY "${CMAKE_BINARY_DIR}")
endif(COVERAGE)
