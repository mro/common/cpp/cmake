# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# include this file at the end of your root CMakeLists.txt file
# optional variables:
#   - LINT_INCDIRS: include directories to take in account, defaults to
#       all *INCLUDE_DIRECTORIES on project's targets.
#   - LINT_SRCDIRS: directory to look for files to lint (recursively),
#       defaults to "src" and "tests".

# Generate compile_commands.json file (for cquery or other linting tools)
if(CMAKE_EXPORT_COMPILE_COMMANDS)
    add_custom_target(compile_commands_copy ALL
        COMMAND ${CMAKE_COMMAND} -E copy "${PROJECT_BINARY_DIR}/compile_commands.json"
            "${PROJECT_SOURCE_DIR}/compile_commands.json"
        DEPENDS "${PROJECT_BINARY_DIR}/compile_commands.json")
endif()

cmake_policy(SET "CMP0057" "NEW") # IN_LIST support

function(_lint_list_compact OUT)
  set(ret)
  foreach(v IN LISTS "${OUT}")
    if(v)
      list(APPEND ret "${v}")
    endif()
  endforeach()
  set("${OUT}" "${ret}" PARENT_SCOPE)
endfunction()

function(_lint_target target OUT)
  get_target_property(inc "${target}" INCLUDE_DIRECTORIES)
  get_target_property(inci "${target}" INTERFACE_INCLUDE_DIRECTORIES)
  get_target_property(incisys "${target}" INTERFACE_SYSTEM_INCLUDE_DIRECTORIES)
  set(incs ${inc} ${inci} ${incisys})
  _lint_list_compact(incs ret)
  if(incs)
    list(APPEND "${OUT}" ${incs})
    list(REMOVE_DUPLICATES "${OUT}")
  endif()

  get_target_property(linklibs "${target}" LINK_LIBRARIES)
  get_target_property(ilinklibs "${target}" INTERFACE_LINK_LIBRARIES)
  set(linklibs ${linklibs} ${ilinklibs})
  foreach(link IN LISTS linklibs)
    if(TARGET "${link}")
      get_target_property(imported "${link}" IMPORTED)
      _lint_target("${link}" "${OUT}")
    endif()
  endforeach()
  set("${OUT}" "${${OUT}}" PARENT_SCOPE)
endfunction()

function(_lint_includes dir OUT)
  get_property(targets DIRECTORY "${dir}" PROPERTY BUILDSYSTEM_TARGETS)
  get_property(subd DIRECTORY "${dir}" PROPERTY SUBDIRECTORIES)
  foreach(target IN LISTS targets)
    _lint_target("${target}" "${OUT}")
  endforeach()
  foreach(sub IN LISTS subd)
    _lint_includes("${sub}" "${OUT}")
  endforeach()
  set("${OUT}" "${${OUT}}" PARENT_SCOPE)
endfunction()

if(NOT DEFINED LINT_INCDIRS)
    _lint_includes("${PROJECT_SOURCE_DIR}" LINT_INCDIRS)
    message(VERBOSE "Linter detected include dirs: ${LINT_INCDIRS}")
endif()

if(NOT DEFINED LINT_SRCDIRS)
    list(APPEND LINT_SRCDIRS src tests)
endif()

function(gen_clangd_config)
    message(STATUS "Generating .clangd ...")
    set(CLANGD_CONFIG_FILE "${PROJECT_SOURCE_DIR}/.clangd")
    file(WRITE "${CLANGD_CONFIG_FILE}" "CompileFlags:\n  Add:\n")
    foreach(DIR ${COMPILER_INCLUDE_DIRS})
        file(APPEND "${CLANGD_CONFIG_FILE}" "    - \"-I${DIR}\"\n")
    endforeach(DIR)

    file(APPEND "${CLANGD_CONFIG_FILE}" "    - \"-I./src\"\n")
    file(APPEND "${CLANGD_CONFIG_FILE}" "    - \"-I${PROJECT_BINARY_DIR}/src\"\n")

    foreach(DIR ${LINT_INCDIRS})
        file(APPEND "${CLANGD_CONFIG_FILE}" "    - \"-I${DIR}\"\n")
    endforeach(DIR)
    foreach(EXTRA ${CLANGD_CONFIG_EXTRA})
        file(APPEND "${CLANGD_CONFIG_FILE}" "    - \"${EXTRA}\"\n")
    endforeach(EXTRA)
endfunction()

function(gen_clang_complete)
    set(CLANG_COMPLETE "${PROJECT_SOURCE_DIR}/.clang_complete")
    message(STATUS "Generating .clang_complete ...")

    file(WRITE "${CLANG_COMPLETE}" "")
    get_cmake_property(ENABLED_LANGUAGES ENABLED_LANGUAGES)
    if("CXX" IN_LIST ENABLED_LANGUAGES AND CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
        file(APPEND "${CLANG_COMPLETE}" "--std=c++11\n")
        execute_process(
            COMMAND "${CMAKE_CXX_COMPILER}" "-xc++" "/dev/null" "-E" "-Wp,-v"
            ERROR_VARIABLE "COMPILER_INCLUDE_DIRS"
            OUTPUT_QUIET)
    elseif("C" IN_LIST ENABLED_LANGUAGES AND CMAKE_C_COMPILER_ID STREQUAL "GNU")
        file(APPEND "${CLANG_COMPLETE}" "--std=gnu99\n")
        execute_process(
            COMMAND "${CMAKE_C_COMPILER}" "-xc" "/dev/null" "-E" "-Wp,-v"
            ERROR_VARIABLE "COMPILER_INCLUDE_DIRS"
            OUTPUT_QUIET)
    else()
        file(APPEND "${CLANG_COMPLETE}" "--std=gnu99\n")
    endif()

    string(REGEX MATCHALL "(\n|^) ([^\r\n]+)\n" COMPILER_INCLUDE_DIRS "${COMPILER_INCLUDE_DIRS}")
    list(TRANSFORM COMPILER_INCLUDE_DIRS STRIP)
    foreach(DIR ${COMPILER_INCLUDE_DIRS})
        file(APPEND "${CLANG_COMPLETE}" "-I${DIR}\n")
    endforeach(DIR)

    file(APPEND "${CLANG_COMPLETE}" "-I./src\n")
    file(APPEND "${CLANG_COMPLETE}" "-I${PROJECT_BINARY_DIR}/src\n")

    foreach(DIR ${LINT_INCDIRS})
        file(APPEND "${CLANG_COMPLETE}" "-I${DIR}\n")
    endforeach(DIR)

    foreach(EXTRA ${CLANG_COMPLETE_EXTRA})
        file(APPEND "${CLANG_COMPLETE}" "${EXTRA}\n")
    endforeach(EXTRA)

    if(GENERATE_CLANGD_CONFIG)
        gen_clangd_config()
    endif()
endfunction()

gen_clang_complete()

include(LintCQuery)

find_program(CLANG_FORMAT_EXE NAMES cquery-clang-format
    HINTS /opt/cquery/bin)
find_program(CLANG_FORMAT_EXE NAMES clang-format)
if(CLANG_FORMAT_EXE)
    message(STATUS "Clang-format found")
    add_custom_target(style
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${LINT_SRCDIRS}"
            -DS="${PROJECT_SOURCE_DIR}" -DB="${PROJECT_BINARY_DIR}"
            -P "${CMAKE_CURRENT_LIST_DIR}/ClangFormatScript.cmake"
        WORKING_DIRECTORY "${PROJECT_SOURCE_DIR}")
else(CLANG_FORMAT_EXE)
    message(STATUS "Clang-format not found")
endif(CLANG_FORMAT_EXE)

# Clang configuration
find_program(CLANGPP_EXE NAMES clang++
    PATHS ${CLANGPP_EXE} ENV PATH $ENV{CLANGPP_PATH}
    CMAKE_FIND_ROOT_PATH_BOTH)
if(CLANGPP_EXE)
    message(STATUS "Clang++ linter found")
    add_custom_target(lint-clang
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${LINT_SRCDIRS}"
            -DS="${PROJECT_SOURCE_DIR}" -DB="${PROJECT_BINARY_DIR}"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintClangScript.cmake"
        WORKING_DIRECTORY "${PROJECT_BINARY_DIR}")
    add_custom_target(lint-clang-clean
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${LINT_SRCDIRS}" -DMODE="clean"
            -DS="${PROJECT_SOURCE_DIR}" -DB="${PROJECT_BINARY_DIR}"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintClangScript.cmake"
        WORKING_DIRECTORY "${PROJECT_BINARY_DIR}")
    list(APPEND LINT_TGT lint-clang)
    list(APPEND LINT_TGT_CLEAN lint-clang-clean)
else(CLANGPP_EXE)
    message(STATUS "Clang++ linter not found")
endif(CLANGPP_EXE)

find_program(CPPCHECK_EXE NAMES cppcheck
    PATHS ${CPPCHECK_EXE} ENV PATH $ENV{CLANGPP_PATH}
    CMAKE_FIND_ROOT_PATH_BOTH)
if(CPPCHECK_EXE)
    message(STATUS "CppCheck linter found")
    add_custom_target(lint-cppcheck
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${LINT_SRCDIRS}"
            -DS="${PROJECT_SOURCE_DIR}" -DB="${PROJECT_BINARY_DIR}"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintCppCheckScript.cmake"
        WORKING_DIRECTORY "${PROJECT_BINARY_DIR}")
    add_custom_target(lint-cppcheck-clean
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${LINT_SRCDIRS}" -DMODE="clean"
            -DS="${PROJECT_SOURCE_DIR}" -DB="${PROJECT_BINARY_DIR}"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintCppCheckScript.cmake"
        WORKING_DIRECTORY "${PROJECT_BINARY_DIR}")
    list(APPEND LINT_TGT lint-cppcheck)
    list(APPEND LINT_TGT_CLEAN lint-cppcheck-clean)
else(CPPCHECK_EXE)
    message(STATUS "CppCheck linter not found")
endif(CPPCHECK_EXE)

find_program(GCC_EXE NAMES gcc g++
    PATHS ${GCC_EXE} ENV PATH $ENV{GCC_PATH}
    CMAKE_FIND_ROOT_PATH_BOTH)
if(GCC_EXE)
    execute_process(
        COMMAND "${GCC_EXE}" --version
        OUTPUT_VARIABLE GCC_VERSION
        ERROR_QUIET)
    if(GCC_VERSION MATCHES " LLVM ")
        message(STATUS "LLVM-gcc not suitable for linting")
        # Looking harder
        unset(GCC_EXE CACHE)
        find_program(GCC_EXE NAMES gcc-7 g++-7 gcc-8 g++-8 gcc-9 g++-9
            PATHS ${GCC_EXE} ENV PATH $ENV{GCC_PATH}
            CMAKE_FIND_ROOT_PATH_BOTH)
    endif()
endif()
if(GCC_EXE)
    message(STATUS "GCC linter found")
    add_custom_target(lint-gcc
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${LINT_SRCDIRS}"
            -DS="${PROJECT_SOURCE_DIR}" -DB="${PROJECT_BINARY_DIR}"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintGCCScript.cmake"
        WORKING_DIRECTORY "${PROJECT_BINARY_DIR}")
    add_custom_target(lint-gcc-clean
        COMMAND ${CMAKE_COMMAND} -DSRCDIRS="${LINT_SRCDIRS}" -DMODE="clean"
            -DS="${PROJECT_SOURCE_DIR}" -DB="${PROJECT_BINARY_DIR}"
            -P "${CMAKE_CURRENT_LIST_DIR}/LintGCCScript.cmake"
        WORKING_DIRECTORY "${PROJECT_BINARY_DIR}")
    list(APPEND LINT_TGT lint-gcc)
    list(APPEND LINT_TGT_CLEAN lint-gcc-clean)
else(GCC_EXE)
    message(STATUS "GCC linter not found")
endif()

if(LINT_TGT)
    add_custom_target(lint DEPENDS ${LINT_TGT})
    add_custom_target(lint-clean DEPENDS ${LINT_TGT_CLEAN})
endif(LINT_TGT)

include(LintBadge)
