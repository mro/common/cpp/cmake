# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

find_package(Doxygen OPTIONAL_COMPONENTS dot)

if(DOXYGEN_FOUND)
  set(DOXYGEN_GENERATE_HTML YES)
  set(DOXYGEN_CALL_GRAPH YES)
  set(DOXYGEN_CALLER_GRAPH YES)
  set(DOXYGEN_SOURCE_BROWSER YES)
  set(DOXYGEN_BUILTIN_STL_SUPPORT YES)
  set(DOXYGEN_JAVADOC_AUTOBRIEF YES)
  set(DOXYGEN_QT_AUTOBRIEF YES)
  set(DOXYGEN_OUTPUT_DIRECTORY "${PROJECT_BINARY_DIR}/doc")

  configure_file("${CMAKE_CURRENT_LIST_DIR}/doxygen_mainpage.md.in" "${PROJECT_BINARY_DIR}/doc/doxygen_mainpage.md" @ONLY)
  set(DOXYGEN_USE_MDFILE_AS_MAINPAGE "doxygen_mainpage.md")

  doxygen_add_docs(doc
    "${PROJECT_BINARY_DIR}/doc/doxygen_mainpage.md" ${PROJECT_SOURCE_DIR}/src
    WORKING_DIRECTORY doc)
endif()
