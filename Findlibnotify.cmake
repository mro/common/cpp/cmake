# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 Sylvain Fargier
# SPDX-Created: 2023-12-09T18:02:32
# SPDX-FileContributor: Author: Sylvain Fargier <fargier.sylvain@gmail.com>

include(FindPackageHandleStandardArgs)

find_package(PkgConfig REQUIRED)
pkg_check_modules(libnotify libnotify)

find_package_handle_standard_args(libnotify
    REQUIRED_VARS libnotify_LINK_LIBRARIES)

add_library(libnotify UNKNOWN IMPORTED)

if(LIBNOTIFY_FOUND)
    list(POP_FRONT libnotify_LINK_LIBRARIES libnotify_LIBRARY)
    set_target_properties(libnotify PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${libnotify_INCLUDE_DIRS}"
        IMPORTED_LINK_INTERFACE_LANGUAGES "C"
        IMPORTED_LOCATION "${libnotify_LIBRARY}"
        IMPORTED_LINK_INTERFACE_LIBRARIES "${libnotify_LINK_LIBRARIES}")
endif()