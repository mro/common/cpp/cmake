# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# Argument: MODE (clean to remove gcda files)
string(TOLOWER "${MODE}" MODE)

get_filename_component(S "${CMAKE_CURRENT_LIST_DIR}/.." ABSOLUTE)
get_filename_component(B "${CMAKE_BINARY_DIR}" ABSOLUTE)

function(run cmd)
    execute_process(COMMAND ${cmd} ${ARGN}
        WORKING_DIRECTORY "${B}"
        ERROR_VARIABLE OUT
        OUTPUT_VARIABLE OUT)
    message("${OUT}")
endfunction()

if(MODE STREQUAL "clean")
    file(GLOB_RECURSE GCDA RELATIVE "${B}" "${B}/*.gcda")
    foreach(F IN LISTS GCDA)
        message(STATUS "Removing ${F}")
        file(REMOVE "${B}/${F}")
    endforeach()
elseif(MODE STREQUAL "result")
    find_program(LCOV_EXE NAMES lcov
        PATHS ${LCOV_PATH} ENV PATH $ENV{LCOV_PATH}
        CMAKE_FIND_ROOT_PATH_BOTH)

    message(STATUS "Displaying result")
    run(${LCOV_EXE} -l ../coverage/lcov.info)
else()
    find_program(LCOV_EXE NAMES lcov
        PATHS ${LCOV_PATH} ENV PATH $ENV{LCOV_PATH}
        CMAKE_FIND_ROOT_PATH_BOTH)

    file(MAKE_DIRECTORY "${S}/coverage")

    macro(moveInfo)
        if(NOT EXISTS "${COVDIR}/temp.info")
            message(FATAL_ERROR "Command failed to generate info file")
        endif()
        file(RENAME "${COVDIR}/temp.info" "${COVDIR}/lcov.info")
    endmacro()

    file(GLOB_RECURSE GCDA RELATIVE "${B}" "${B}/*.gcda")
    if(NOT GCDA)
        message(FATAL_ERROR "No GCDA files found, please generate some info")
    endif()

    set(COVDIR "${S}/coverage")
    message(STATUS "Generating lcov info")
    run(${LCOV_EXE} --quiet ${LCOV_OPTIONS} -c -d . -o "${COVDIR}/temp.info")
    moveInfo()

    message(STATUS "Removing external coverage info")
    set(PATTERNS ${SRCDIRS})
    list(TRANSFORM PATTERNS REPLACE "/?(.+)/?" "*/\\1/*")

    run(${LCOV_EXE} --quiet ${LCOV_OPTIONS} -e "${COVDIR}/lcov.info" -o "${COVDIR}/temp.info" ${PATTERNS})
    moveInfo()

    find_program(SED_EXE NAMES sed)
    if(SED_EXE)
        message(STATUS "Making path relative")
        run(${SED_EXE} -i "s#${S}/src#src#" "${COVDIR}/lcov.info")
    endif()
endif()
