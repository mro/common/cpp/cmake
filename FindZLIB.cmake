# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2024 CERN (home.cern)
# SPDX-Created: 2024-03-27T15:44:25
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

include(FindPackageHandleStandardArgs)
include(${CMAKE_CURRENT_LIST_DIR}/Tools.cmake)

if(NOT ZLIB_FOUND)
    set(_ZLIB_FIND_REQUIRED "${ZLIB_FIND_REQUIRED}")
    unset(ZLIB_FIND_REQUIRED)

    set(_CMAKE_MODULE_PATH "${CMAKE_MODULE_PATH}")
    unset(CMAKE_MODULE_PATH)

    # Recurse for other packages
    find_package(ZLIB)

    set(CMAKE_MODULE_PATH "${_CMAKE_MODULE_PATH}")
    unset(_CMAKE_MODULE_PATH)

    set(ZLIB_FIND_REQUIRED "${_ZLIB_FIND_REQUIRED}")
    unset(_ZLIB_FIND_REQUIRED)
endif()

if((ZLIB_LOCAL OR NOT ZLIB_FOUND) AND ZLIB_REPO_DIR AND NOT TARGET zlib)
    if(NOT ZLIB_FIND_QUIETLY)
        message(STATUS "  Using local version of 'ZLIB'")
    endif()
    set(ZLIB_LOCAL TRUE CACHE BOOL "local build" FORCE)
    set(ZLIB_FOUND TRUE CACHE BOOL "local build" FORCE)
    set_default(ZLIB_REMOTE_URL "https://github.com/madler/zlib.git")
    set_default(ZLIB_REMOTE_REF "master")

    if(NOT EXISTS ${ZLIB_REPO_DIR})
        find_package(Git REQUIRED)
        get_filename_component(DEPS_DIR "${ZLIB_REPO_DIR}" DIRECTORY)
        file(MAKE_DIRECTORY "${DEPS_DIR}")
        execute_process(COMMAND ${GIT_EXECUTABLE} clone -b "${ZLIB_REMOTE_REF}"
            "${ZLIB_REMOTE_URL}" "${ZLIB_REPO_DIR}"
            RESULT_VARIABLE RET)
        assert(RET EQUAL 0 MESSAGE "Failed to clone ${ZLIB_REMOTE_URL}" FATAL_ERROR)
    endif()
    file(GLOB ZLIB_SRC RELATIVE "${CMAKE_CURRENT_SOURCE_DIR}"
        "${ZLIB_REPO_DIR}/*.c"
        "${ZLIB_REPO_DIR}/*.h")
    set(ZLIB_INCLUDE_DIR "${ZLIB_REPO_DIR}" CACHE PATH "Local ZLIB path" FORCE)
    set(ZLIB_LIBRARY_DEBUG zlib CACHE PATH "Local ZLIB library" FORCE)
    set(ZLIB_LIBRARY_RELEASE zlib CACHE PATH "Local ZLIB library" FORCE)
    set(ZLIB_LIBRARIES zlib CACHE PATH "Local ZLIB library" FORCE)

    add_library(zlib STATIC ${ZLIB_SRC})
    target_include_directories(zlib PRIVATE "${ZLIB_INCLUDE_DIR}")

    add_library(ZLIB::ZLIB UNKNOWN IMPORTED)
    set_target_properties(ZLIB::ZLIB PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES "${ZLIB_INCLUDE_DIR}"
        IMPORTED_LINK_INTERFACE_LANGUAGES "C"
        IMPORTED_LOCATION "${CMAKE_CURRENT_BINARY_DIR}/libzlib.a"
        IMPORTED_LINK_INTERFACE_LIBRARIES "${ZLIB_LIBRARIES}")
endif()

find_package_handle_standard_args(ZLIB
    REQUIRED_VARS ZLIB_LIBRARIES ZLIB_INCLUDE_DIR)
